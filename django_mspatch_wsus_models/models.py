from django.db import models


class Ivwapiupdaterevision(models.Model):
    updateid = models.CharField(
        db_column="UpdateID", max_length=36
    )  # Field name made lowercase.
    revisionnumber = models.IntegerField(
        db_column="RevisionNumber"
    )  # Field name made lowercase.
    localupdateid = models.IntegerField(
        db_column="LocalUpdateID"
    )  # Field name made lowercase.
    revisionid = models.IntegerField(
        db_column="RevisionID"
    )  # Field name made lowercase.
    islatestrevision = models.BooleanField(
        db_column="IsLatestRevision"
    )  # Field name made lowercase.
    ishidden = models.BooleanField(db_column="IsHidden")  # Field name made lowercase.
    updatetype = models.CharField(
        db_column="UpdateType", max_length=256
    )  # Field name made lowercase.
    islocallypublished = models.BooleanField(
        db_column="IsLocallyPublished"
    )  # Field name made lowercase.
    defaultpropertieslanguageid = models.IntegerField(
        db_column="DefaultPropertiesLanguageID", blank=True, null=True
    )  # Field name made lowercase.
    ismandatory = models.BooleanField(
        db_column="IsMandatory"
    )  # Field name made lowercase.
    effectivearrivaltime = models.DateTimeField(
        db_column="EffectiveArrivalTime"
    )  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = "ivwApiUpdateRevision"


class Tbauthorization(models.Model):
    pluginid = models.CharField(
        db_column="PluginID", primary_key=True, max_length=128
    )  # Field name made lowercase.
    serviceurl = models.CharField(
        db_column="ServiceURL", max_length=256
    )  # Field name made lowercase.
    assemblyname = models.CharField(
        db_column="AssemblyName", max_length=128
    )  # Field name made lowercase.
    classname = models.CharField(
        db_column="ClassName", max_length=128
    )  # Field name made lowercase.
    parameters = models.CharField(
        db_column="Parameters", max_length=500, blank=True, null=True
    )  # Field name made lowercase.
    authorizationdata = models.BinaryField(
        db_column="AuthorizationData", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbAuthorization"


class Tbautodeploymentrule(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    enabled = models.BooleanField(db_column="Enabled")  # Field name made lowercase.
    actionid = models.IntegerField(db_column="ActionID")  # Field name made lowercase.
    name = models.CharField(
        db_column="Name", max_length=256
    )  # Field name made lowercase.
    dateoffset = models.SmallIntegerField(
        db_column="DateOffset", blank=True, null=True
    )  # Field name made lowercase.
    minutesaftermidnight = models.SmallIntegerField(
        db_column="MinutesAfterMidnight", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbAutoDeploymentRule"


class Tbbundleall(models.Model):
    revisionid = models.ForeignKey(
        "Tbrevision", models.DO_NOTHING, db_column="RevisionID"
    )  # Field name made lowercase.
    bundledid = models.AutoField(
        db_column="BundledID", primary_key=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbBundleAll"


class Tbbundleatleastone(models.Model):
    revisionid = models.OneToOneField(
        "Tbrevision", models.DO_NOTHING, db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    bundledid = models.ForeignKey(
        Tbbundleall, models.DO_NOTHING, db_column="BundledID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbBundleAtLeastOne"
        unique_together = (("revisionid", "bundledid"),)


class Tbbundledependency(models.Model):
    revisionid = models.IntegerField(
        db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    bundledrevisionid = models.IntegerField(
        db_column="BundledRevisionID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbBundleDependency"
        unique_together = (("revisionid", "bundledrevisionid"),)


class Tbcategory(models.Model):
    categoryindex = models.AutoField(
        db_column="CategoryIndex"
    )  # Field name made lowercase.
    categoryid = models.OneToOneField(
        "Tbupdate", models.DO_NOTHING, db_column="CategoryID", primary_key=True
    )  # Field name made lowercase.
    parentcategoryid = models.ForeignKey(
        "self", models.DO_NOTHING, db_column="ParentCategoryID", blank=True, null=True
    )  # Field name made lowercase.
    categorytype = models.ForeignKey(
        "Tbcategorytype", models.DO_NOTHING, db_column="CategoryType"
    )  # Field name made lowercase.
    lastchange = models.DateTimeField(
        db_column="LastChange"
    )  # Field name made lowercase.
    prohibitssubcategories = models.BooleanField(
        db_column="ProhibitsSubcategories"
    )  # Field name made lowercase.
    prohibitsupdates = models.BooleanField(
        db_column="ProhibitsUpdates"
    )  # Field name made lowercase.
    displayorder = models.IntegerField(
        db_column="DisplayOrder", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbCategory"


class Tbcategoryinautodeploymentrule(models.Model):
    autodeploymentruleid = models.OneToOneField(
        Tbautodeploymentrule,
        models.DO_NOTHING,
        db_column="AutoDeploymentRuleID",
        primary_key=True,
    )  # Field name made lowercase.
    categoryid = models.ForeignKey(
        "Tbupdate", models.DO_NOTHING, db_column="CategoryID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbCategoryInAutoDeploymentRule"
        unique_together = (("autodeploymentruleid", "categoryid"),)


class Tbcategoryinsubscription(models.Model):
    subscriptionid = models.IntegerField(
        db_column="SubscriptionID"
    )  # Field name made lowercase.
    categoryid = models.OneToOneField(
        Tbcategory, models.DO_NOTHING, db_column="CategoryID", primary_key=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbCategoryInSubscription"
        unique_together = (("categoryid", "subscriptionid"),)


class Tbcategorytype(models.Model):
    categorytype = models.CharField(
        db_column="CategoryType", primary_key=True, max_length=256
    )  # Field name made lowercase.
    level = models.IntegerField(db_column="Level")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbCategoryType"


class Tbchangetracking(models.Model):
    notificationeventname = models.CharField(
        db_column="NotificationEventName", primary_key=True, max_length=256
    )  # Field name made lowercase.
    changenumber = models.BigIntegerField(
        db_column="ChangeNumber"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbChangeTracking"


class Tbclientwithrecentnamechange(models.Model):
    computerid = models.CharField(
        db_column="ComputerID", max_length=256
    )  # Field name made lowercase.
    fulldomainname = models.CharField(
        db_column="FullDomainName", max_length=256
    )  # Field name made lowercase.
    lastchanged = models.DateTimeField(
        db_column="LastChanged"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbClientWithRecentNameChange"


class Tbcompatibleprinterprovider(models.Model):
    revisionid = models.OneToOneField(
        "Tbrevision", models.DO_NOTHING, db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    compatibleprovider = models.CharField(
        db_column="CompatibleProvider", max_length=256
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbCompatiblePrinterProvider"
        unique_together = (("revisionid", "compatibleprovider"),)


class Tbcomputersummaryformicrosoftupdates(models.Model):
    targetid = models.OneToOneField(
        "Tbcomputertarget", models.DO_NOTHING, db_column="TargetID", primary_key=True
    )  # Field name made lowercase.
    unknown = models.IntegerField(db_column="Unknown")  # Field name made lowercase.
    notinstalled = models.IntegerField(
        db_column="NotInstalled"
    )  # Field name made lowercase.
    downloaded = models.IntegerField(
        db_column="Downloaded"
    )  # Field name made lowercase.
    installed = models.IntegerField(db_column="Installed")  # Field name made lowercase.
    failed = models.IntegerField(db_column="Failed")  # Field name made lowercase.
    installedpendingreboot = models.IntegerField(
        db_column="InstalledPendingReboot"
    )  # Field name made lowercase.
    lastchangetime = models.DateTimeField(
        db_column="LastChangeTime"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbComputerSummaryForMicrosoftUpdates"


class Tbcomputertarget(models.Model):
    targetid = models.OneToOneField(
        "Tbtarget", models.DO_NOTHING, db_column="TargetID", primary_key=True
    )  # Field name made lowercase.
    computerid = models.CharField(
        db_column="ComputerID", unique=True, max_length=256
    )  # Field name made lowercase.
    sid = models.BinaryField(
        db_column="SID", blank=True, null=True
    )  # Field name made lowercase.
    lastsynctime = models.DateTimeField(
        db_column="LastSyncTime", blank=True, null=True
    )  # Field name made lowercase.
    lastreportedstatustime = models.DateTimeField(
        db_column="LastReportedStatusTime", blank=True, null=True
    )  # Field name made lowercase.
    lastreportedreboottime = models.DateTimeField(
        db_column="LastReportedRebootTime", blank=True, null=True
    )  # Field name made lowercase.
    ipaddress = models.CharField(
        db_column="IPAddress", max_length=56, blank=True, null=True
    )  # Field name made lowercase.
    fulldomainname = models.CharField(
        db_column="FullDomainName", unique=True, max_length=255, blank=True, null=True
    )  # Field name made lowercase.
    isregistered = models.BooleanField(
        db_column="IsRegistered"
    )  # Field name made lowercase.
    lastinventorytime = models.DateTimeField(
        db_column="LastInventoryTime", blank=True, null=True
    )  # Field name made lowercase.
    lastnamechangetime = models.DateTimeField(
        db_column="LastNameChangeTime", blank=True, null=True
    )  # Field name made lowercase.
    effectivelastdetectiontime = models.DateTimeField(
        db_column="EffectiveLastDetectionTime", blank=True, null=True
    )  # Field name made lowercase.
    parentservertargetid = models.ForeignKey(
        "Tbdownstreamservertarget",
        models.DO_NOTHING,
        db_column="ParentServerTargetID",
        blank=True,
        null=True,
    )  # Field name made lowercase.
    lastsyncresult = models.IntegerField(
        db_column="LastSyncResult"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbComputerTarget"


class Tbcomputertargetdetail(models.Model):
    targetid = models.IntegerField(
        db_column="TargetID", primary_key=True
    )  # Field name made lowercase.
    osmajorversion = models.IntegerField(
        db_column="OSMajorVersion", blank=True, null=True
    )  # Field name made lowercase.
    osminorversion = models.IntegerField(
        db_column="OSMinorVersion", blank=True, null=True
    )  # Field name made lowercase.
    osbuildnumber = models.IntegerField(
        db_column="OSBuildNumber", blank=True, null=True
    )  # Field name made lowercase.
    osservicepackmajornumber = models.IntegerField(
        db_column="OSServicePackMajorNumber", blank=True, null=True
    )  # Field name made lowercase.
    osservicepackminornumber = models.IntegerField(
        db_column="OSServicePackMinorNumber", blank=True, null=True
    )  # Field name made lowercase.
    oslocale = models.CharField(
        db_column="OSLocale", max_length=10, blank=True, null=True
    )  # Field name made lowercase.
    computermake = models.CharField(
        db_column="ComputerMake", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    computermodel = models.CharField(
        db_column="ComputerModel", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    biosversion = models.CharField(
        db_column="BiosVersion", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    biosname = models.CharField(
        db_column="BiosName", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    biosreleasedate = models.DateTimeField(
        db_column="BiosReleaseDate", blank=True, null=True
    )  # Field name made lowercase.
    processorarchitecture = models.CharField(
        db_column="ProcessorArchitecture", max_length=50, blank=True, null=True
    )  # Field name made lowercase.
    laststatusrolluptime = models.DateTimeField(
        db_column="LastStatusRollupTime", blank=True, null=True
    )  # Field name made lowercase.
    lastreceivedstatusrollupnumber = models.IntegerField(
        db_column="LastReceivedStatusRollupNumber"
    )  # Field name made lowercase.
    lastsentstatusrollupnumber = models.IntegerField(
        db_column="LastSentStatusRollupNumber"
    )  # Field name made lowercase.
    samplingvalue = models.IntegerField(
        db_column="SamplingValue"
    )  # Field name made lowercase.
    createdtime = models.DateTimeField(
        db_column="CreatedTime"
    )  # Field name made lowercase.
    suitemask = models.SmallIntegerField(
        db_column="SuiteMask"
    )  # Field name made lowercase.
    oldproducttype = models.SmallIntegerField(
        db_column="OldProductType"
    )  # Field name made lowercase.
    newproducttype = models.IntegerField(
        db_column="NewProductType"
    )  # Field name made lowercase.
    systemmetrics = models.IntegerField(
        db_column="SystemMetrics"
    )  # Field name made lowercase.
    clientversion = models.CharField(
        db_column="ClientVersion", max_length=23, blank=True, null=True
    )  # Field name made lowercase.
    targetgroupmembershipchanged = models.BooleanField(
        db_column="TargetGroupMembershipChanged"
    )  # Field name made lowercase.
    osfamily = models.CharField(
        db_column="OSFamily", max_length=256
    )  # Field name made lowercase.
    osdescription = models.CharField(
        db_column="OSDescription", max_length=256, blank=True, null=True
    )  # Field name made lowercase.
    oem = models.CharField(
        db_column="OEM", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    devicetype = models.CharField(
        db_column="DeviceType", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    firmwareversion = models.CharField(
        db_column="FirmwareVersion", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    mobileoperator = models.CharField(
        db_column="MobileOperator", max_length=15, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbComputerTargetDetail"


class Tbcomputersthatneeddetailedrollup(models.Model):
    targetid = models.OneToOneField(
        Tbcomputertarget, models.DO_NOTHING, db_column="TargetID", primary_key=True
    )  # Field name made lowercase.
    isbeingrolledup = models.BooleanField(
        db_column="IsBeingRolledUp"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbComputersThatNeedDetailedRollup"


class Tbconfiguration(models.Model):
    name = models.CharField(
        db_column="Name", primary_key=True, max_length=128
    )  # Field name made lowercase.
    value = models.TextField(
        db_column="Value"
    )  # Field name made lowercase. This field type is a guess.

    class Meta:
        managed = False
        db_table = "tbConfiguration"


class Tbconfigurationa(models.Model):
    configurationid = models.AutoField(
        db_column="ConfigurationID", primary_key=True
    )  # Field name made lowercase.
    lastconfigchange = models.DateTimeField(
        db_column="LastConfigChange"
    )  # Field name made lowercase.
    dssanonymoustargeting = models.BooleanField(
        db_column="DssAnonymousTargeting"
    )  # Field name made lowercase.
    isregistrationrequired = models.BooleanField(
        db_column="IsRegistrationRequired"
    )  # Field name made lowercase.
    maxdeltasyncperiod = models.IntegerField(
        db_column="MaxDeltaSyncPeriod"
    )  # Field name made lowercase.
    reportingserviceurl = models.CharField(
        db_column="ReportingServiceUrl", max_length=1024
    )  # Field name made lowercase.
    serverid = models.CharField(
        db_column="ServerID", max_length=36
    )  # Field name made lowercase.
    anonymouscookieexpirationtime = models.BigIntegerField(
        db_column="AnonymousCookieExpirationTime", blank=True, null=True
    )  # Field name made lowercase.
    simpletargetingcookieexpirationtime = models.BigIntegerField(
        db_column="SimpleTargetingCookieExpirationTime"
    )  # Field name made lowercase.
    maximumservercookieexpirationtime = models.BigIntegerField(
        db_column="MaximumServerCookieExpirationTime"
    )  # Field name made lowercase.
    dsstargetingcookieexpirationtime = models.BigIntegerField(
        db_column="DssTargetingCookieExpirationTime"
    )  # Field name made lowercase.
    encryptionkey = models.BinaryField(
        db_column="EncryptionKey"
    )  # Field name made lowercase.
    servertargeting = models.BooleanField(
        db_column="ServerTargeting"
    )  # Field name made lowercase.
    synctomu = models.BooleanField(db_column="SyncToMU")  # Field name made lowercase.
    upstreamservername = models.CharField(
        db_column="UpstreamServerName", max_length=256
    )  # Field name made lowercase.
    serverportnumber = models.IntegerField(
        db_column="ServerPortNumber"
    )  # Field name made lowercase.
    upstreamserverusessl = models.BooleanField(
        db_column="UpstreamServerUseSSL"
    )  # Field name made lowercase.
    useproxy = models.BooleanField(db_column="UseProxy")  # Field name made lowercase.
    proxyname = models.CharField(
        db_column="ProxyName", max_length=256
    )  # Field name made lowercase.
    proxyserverport = models.IntegerField(
        db_column="ProxyServerPort"
    )  # Field name made lowercase.
    anonymousproxyaccess = models.BooleanField(
        db_column="AnonymousProxyAccess"
    )  # Field name made lowercase.
    proxyusername = models.CharField(
        db_column="ProxyUserName", max_length=256
    )  # Field name made lowercase.
    proxypassword = models.CharField(
        db_column="ProxyPassword", max_length=728
    )  # Field name made lowercase.
    hostonmu = models.BooleanField(db_column="HostOnMu")  # Field name made lowercase.
    handshakeanchor = models.CharField(
        db_column="HandshakeAnchor", max_length=64, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbConfigurationA"


class Tbconfigurationb(models.Model):
    configurationid = models.OneToOneField(
        Tbconfigurationa,
        models.DO_NOTHING,
        db_column="ConfigurationID",
        primary_key=True,
    )  # Field name made lowercase.
    localcontentcachelocation = models.CharField(
        db_column="LocalContentCacheLocation", max_length=256
    )  # Field name made lowercase.
    serversupportsalllanguages = models.BooleanField(
        db_column="ServerSupportsAllLanguages"
    )  # Field name made lowercase.
    loglevel = models.IntegerField(db_column="LogLevel")  # Field name made lowercase.
    logpath = models.CharField(
        db_column="LogPath", max_length=256
    )  # Field name made lowercase.
    subscriptionfailurenumberofretries = models.IntegerField(
        db_column="SubscriptionFailureNumberOfRetries"
    )  # Field name made lowercase.
    subscriptionfailurewaitbetweenretriestime = models.BigIntegerField(
        db_column="SubscriptionFailureWaitBetweenRetriesTime"
    )  # Field name made lowercase.
    dispatchmanagerpollinginterval = models.BigIntegerField(
        db_column="DispatchManagerPollingInterval"
    )  # Field name made lowercase.
    statemachinetransitionloggingenabled = models.BooleanField(
        db_column="StateMachineTransitionLoggingEnabled"
    )  # Field name made lowercase.
    statemachinetransitionerrorcapturelength = models.BigIntegerField(
        db_column="StateMachineTransitionErrorCaptureLength"
    )  # Field name made lowercase.
    maxsimultaneousfiledownloads = models.IntegerField(
        db_column="MaxSimultaneousFileDownloads"
    )  # Field name made lowercase.
    muurl = models.CharField(
        db_column="MUUrl", max_length=1024
    )  # Field name made lowercase.
    maxnumberofidstorequestdatafromuss = models.IntegerField(
        db_column="MaxNumberOfIdsToRequestDataFromUss"
    )  # Field name made lowercase.
    eventlogfloodprotecttime = models.BigIntegerField(
        db_column="EventLogFloodProtectTime"
    )  # Field name made lowercase.
    doreportingdatavalidation = models.BooleanField(
        db_column="DoReportingDataValidation"
    )  # Field name made lowercase.
    doreportingsummarization = models.BooleanField(
        db_column="DoReportingSummarization"
    )  # Field name made lowercase.
    statsdotnetwebserviceuri = models.CharField(
        db_column="StatsDotNetWebServiceUri", max_length=1024
    )  # Field name made lowercase.
    queueflushtimeinms = models.IntegerField(
        db_column="QueueFlushTimeInMS"
    )  # Field name made lowercase.
    queueflushcount = models.IntegerField(
        db_column="QueueFlushCount"
    )  # Field name made lowercase.
    queuerejectcount = models.IntegerField(
        db_column="QueueRejectCount"
    )  # Field name made lowercase.
    sleeptimeaftererrorinms = models.IntegerField(
        db_column="SleepTimeAfterErrorInMS"
    )  # Field name made lowercase.
    logdestinations = models.IntegerField(
        db_column="LogDestinations"
    )  # Field name made lowercase.
    autorefreshdeployments = models.BooleanField(
        db_column="AutoRefreshDeployments"
    )  # Field name made lowercase.
    redirectorchangenumber = models.BigIntegerField(
        db_column="RedirectorChangeNumber"
    )  # Field name made lowercase.
    importlocalpath = models.CharField(
        db_column="ImportLocalPath", max_length=256
    )  # Field name made lowercase.
    usecookievalidation = models.BooleanField(
        db_column="UseCookieValidation"
    )  # Field name made lowercase.
    autopurgedetectionperiod = models.IntegerField(
        db_column="AutoPurgeDetectionPeriod", blank=True, null=True
    )  # Field name made lowercase.
    autopurgeclienteventagethreshold = models.IntegerField(
        db_column="AutoPurgeClientEventAgeThreshold", blank=True, null=True
    )  # Field name made lowercase.
    autopurgeservereventagethreshold = models.IntegerField(
        db_column="AutoPurgeServerEventAgeThreshold", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbConfigurationB"


class Tbconfigurationc(models.Model):
    configurationid = models.OneToOneField(
        Tbconfigurationa,
        models.DO_NOTHING,
        db_column="ConfigurationID",
        primary_key=True,
    )  # Field name made lowercase.
    maxcoreupdatesperrequest = models.IntegerField(
        db_column="MaxCoreUpdatesPerRequest"
    )  # Field name made lowercase.
    maxextendedupdatesperrequest = models.IntegerField(
        db_column="MaxExtendedUpdatesPerRequest"
    )  # Field name made lowercase.
    downloadregulationurl = models.CharField(
        db_column="DownloadRegulationUrl", max_length=1024
    )  # Field name made lowercase.
    allowproxycredentialsovernonssl = models.BooleanField(
        db_column="AllowProxyCredentialsOverNonSsl"
    )  # Field name made lowercase.
    lazysync = models.BooleanField(db_column="LazySync")  # Field name made lowercase.
    downloadexpresspackages = models.BooleanField(
        db_column="DownloadExpressPackages"
    )  # Field name made lowercase.
    doserversynccompression = models.BooleanField(
        db_column="DoServerSyncCompression"
    )  # Field name made lowercase.
    proxyuserdomain = models.CharField(
        db_column="ProxyUserDomain", max_length=256
    )  # Field name made lowercase.
    bitshealthscanninginterval = models.BigIntegerField(
        db_column="BitsHealthScanningInterval"
    )  # Field name made lowercase.
    bitsdownloadpriorityforeground = models.BooleanField(
        db_column="BitsDownloadPriorityForeground"
    )  # Field name made lowercase.
    maxxmlperrequest = models.IntegerField(
        db_column="MaxXmlPerRequest"
    )  # Field name made lowercase.
    maxxmlperrequestinserversync = models.IntegerField(
        db_column="MaxXmlPerRequestInServerSync"
    )  # Field name made lowercase.
    maxtargetcomputers = models.IntegerField(
        db_column="MaxTargetComputers"
    )  # Field name made lowercase.
    maxeventinstances = models.IntegerField(
        db_column="MaxEventInstances"
    )  # Field name made lowercase.
    replicamode = models.BooleanField(
        db_column="ReplicaMode"
    )  # Field name made lowercase.
    logrolloverfilesizeinbytes = models.IntegerField(
        db_column="LogRolloverFileSizeInBytes"
    )  # Field name made lowercase.
    autodeploymandatory = models.BooleanField(
        db_column="AutoDeployMandatory"
    )  # Field name made lowercase.
    wusinstalltype = models.IntegerField(
        db_column="WUSInstallType"
    )  # Field name made lowercase.
    deploymentchangedeferral = models.IntegerField(
        db_column="DeploymentChangeDeferral"
    )  # Field name made lowercase.
    revisiondeletiontimethreshold = models.IntegerField(
        db_column="RevisionDeletionTimeThreshold"
    )  # Field name made lowercase.
    revisiondeletionsizethreshold = models.IntegerField(
        db_column="RevisionDeletionSizeThreshold"
    )  # Field name made lowercase.
    corexmlcompressionthreshold = models.IntegerField(
        db_column="CoreXmlCompressionThreshold"
    )  # Field name made lowercase.
    publishedxmlcompressionthreshold = models.IntegerField(
        db_column="PublishedXmlCompressionThreshold"
    )  # Field name made lowercase.
    maxdownstreamservers = models.IntegerField(
        db_column="MaxDownstreamServers"
    )  # Field name made lowercase.
    collectclientinventory = models.BooleanField(
        db_column="CollectClientInventory"
    )  # Field name made lowercase.
    dodetailedrollup = models.BooleanField(
        db_column="DoDetailedRollup"
    )  # Field name made lowercase.
    rollupresetguid = models.CharField(
        db_column="RollupResetGuid", max_length=36
    )  # Field name made lowercase.
    hmdetectintervalinseconds = models.IntegerField(
        db_column="HmDetectIntervalInSeconds"
    )  # Field name made lowercase.
    hmrefreshintervalinseconds = models.IntegerField(
        db_column="HmRefreshIntervalInSeconds"
    )  # Field name made lowercase.
    hmcorediskspacegreenmegabytes = models.IntegerField(
        db_column="HmCoreDiskSpaceGreenMegabytes"
    )  # Field name made lowercase.
    hmcorediskspaceredmegabytes = models.IntegerField(
        db_column="HmCoreDiskSpaceRedMegabytes"
    )  # Field name made lowercase.
    hmcorecatalogsyncintervalindays = models.IntegerField(
        db_column="HmCoreCatalogSyncIntervalInDays"
    )  # Field name made lowercase.
    hmclientsinstallupdatesgreenpercent = models.IntegerField(
        db_column="HmClientsInstallUpdatesGreenPercent"
    )  # Field name made lowercase.
    hmclientsinstallupdatesredpercent = models.IntegerField(
        db_column="HmClientsInstallUpdatesRedPercent"
    )  # Field name made lowercase.
    hmclientsinventorygreenpercent = models.IntegerField(
        db_column="HmClientsInventoryGreenPercent"
    )  # Field name made lowercase.
    hmclientsinventoryredpercent = models.IntegerField(
        db_column="HmClientsInventoryRedPercent"
    )  # Field name made lowercase.
    hmclientsinventoryscandiffinhours = models.IntegerField(
        db_column="HmClientsInventoryScanDiffInHours"
    )  # Field name made lowercase.
    hmclientssilentgreenpercent = models.IntegerField(
        db_column="HmClientsSilentGreenPercent"
    )  # Field name made lowercase.
    hmclientssilentredpercent = models.IntegerField(
        db_column="HmClientsSilentRedPercent"
    )  # Field name made lowercase.
    hmclientssilentdays = models.IntegerField(
        db_column="HmClientsSilentDays"
    )  # Field name made lowercase.
    hmcoreflags = models.IntegerField(
        db_column="HmCoreFlags"
    )  # Field name made lowercase.
    hmclientsflags = models.IntegerField(
        db_column="HmClientsFlags"
    )  # Field name made lowercase.
    hmdatabaseflags = models.IntegerField(
        db_column="HmDatabaseFlags"
    )  # Field name made lowercase.
    hmwebservicesflags = models.IntegerField(
        db_column="HmWebServicesFlags"
    )  # Field name made lowercase.
    hmclientstoomanygreenpercent = models.IntegerField(
        db_column="HmClientsTooManyGreenPercent"
    )  # Field name made lowercase.
    hmclientstoomanyredpercent = models.IntegerField(
        db_column="HmClientsTooManyRedPercent"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbConfigurationC"


class Tbdcrollupstatus(models.Model):
    id = models.OneToOneField(
        "Tbtarget", models.DO_NOTHING, db_column="Id", primary_key=True
    )  # Field name made lowercase.
    lastreport = models.DateTimeField(
        db_column="LastReport"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbDcRollupStatus"


class Tbdeaddeployment(models.Model):
    deploymentid = models.IntegerField(
        db_column="DeploymentID", primary_key=True
    )  # Field name made lowercase.
    timeofdeath = models.DateTimeField(
        db_column="TimeOfDeath"
    )  # Field name made lowercase.
    actionid = models.IntegerField(db_column="ActionID")  # Field name made lowercase.
    deploymenttime = models.DateTimeField(
        db_column="DeploymentTime"
    )  # Field name made lowercase.
    golivetime = models.DateTimeField(
        db_column="GoLiveTime"
    )  # Field name made lowercase.
    deadline = models.DateTimeField(
        db_column="Deadline", blank=True, null=True
    )  # Field name made lowercase.
    adminname = models.CharField(
        db_column="AdminName", max_length=385
    )  # Field name made lowercase.
    downloadpriority = models.SmallIntegerField(
        db_column="DownloadPriority"
    )  # Field name made lowercase.
    deploymentguid = models.CharField(
        db_column="DeploymentGuid", max_length=36
    )  # Field name made lowercase.
    isassigned = models.BooleanField(
        db_column="IsAssigned"
    )  # Field name made lowercase.
    revisionid = models.IntegerField(
        db_column="RevisionID"
    )  # Field name made lowercase.
    targetgroupid = models.CharField(
        db_column="TargetGroupID", max_length=36
    )  # Field name made lowercase.
    targetgrouptypeid = models.SmallIntegerField(
        db_column="TargetGroupTypeID"
    )  # Field name made lowercase.
    adminnamewhodeleted = models.CharField(
        db_column="AdminNameWhoDeleted", max_length=385
    )  # Field name made lowercase.
    lastchangenumber = models.BigIntegerField(
        db_column="LastChangeNumber"
    )  # Field name made lowercase.
    updateid = models.CharField(
        db_column="UpdateID", max_length=36
    )  # Field name made lowercase.
    revisionnumber = models.IntegerField(
        db_column="RevisionNumber"
    )  # Field name made lowercase.
    updatetype = models.CharField(
        db_column="UpdateType", max_length=256
    )  # Field name made lowercase.
    ispartofset = models.BooleanField(
        db_column="IsPartOfSet"
    )  # Field name made lowercase.
    setcreationtime = models.DateTimeField(
        db_column="SetCreationTime", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbDeadDeployment"


class Tbdeletedcomputer(models.Model):
    computerid = models.CharField(
        db_column="ComputerID", primary_key=True, max_length=256
    )  # Field name made lowercase.
    deletedtime = models.DateTimeField(
        db_column="DeletedTime"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbDeletedComputer"


class Tbdeleteddynamiccategory(models.Model):
    id = models.CharField(
        db_column="Id", primary_key=True, max_length=36
    )  # Field name made lowercase.
    deleted = models.DateTimeField(db_column="Deleted")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbDeletedDynamicCategory"


class Tbdeployment(models.Model):
    deploymentid = models.AutoField(
        db_column="DeploymentID", primary_key=True
    )  # Field name made lowercase.
    lastchangetime = models.DateTimeField(
        db_column="LastChangeTime"
    )  # Field name made lowercase.
    lastchangenumber = models.BigIntegerField(
        db_column="LastChangeNumber"
    )  # Field name made lowercase.
    deploymentstatus = models.SmallIntegerField(
        db_column="DeploymentStatus"
    )  # Field name made lowercase.
    actionid = models.IntegerField(db_column="ActionID")  # Field name made lowercase.
    deploymenttime = models.DateTimeField(
        db_column="DeploymentTime"
    )  # Field name made lowercase.
    golivetime = models.DateTimeField(
        db_column="GoLiveTime"
    )  # Field name made lowercase.
    deadline = models.DateTimeField(
        db_column="Deadline", blank=True, null=True
    )  # Field name made lowercase.
    adminname = models.CharField(
        db_column="AdminName", max_length=385
    )  # Field name made lowercase.
    downloadpriority = models.SmallIntegerField(
        db_column="DownloadPriority"
    )  # Field name made lowercase.
    deploymentguid = models.CharField(
        db_column="DeploymentGuid", max_length=36
    )  # Field name made lowercase.
    isassigned = models.BooleanField(
        db_column="IsAssigned"
    )  # Field name made lowercase.
    revisionid = models.ForeignKey(
        "Tbrevision", models.DO_NOTHING, db_column="RevisionID"
    )  # Field name made lowercase.
    targetgroupid = models.ForeignKey(
        "Tbtargetgroup", models.DO_NOTHING, db_column="TargetGroupID"
    )  # Field name made lowercase.
    targetgrouptypeid = models.SmallIntegerField(
        db_column="TargetGroupTypeID"
    )  # Field name made lowercase.
    isleaf = models.BooleanField(db_column="IsLeaf")  # Field name made lowercase.
    updatetype = models.CharField(
        db_column="UpdateType", max_length=256
    )  # Field name made lowercase.
    iscritical = models.BooleanField(
        db_column="IsCritical"
    )  # Field name made lowercase.
    priority = models.IntegerField(db_column="Priority")  # Field name made lowercase.
    isfeatured = models.BooleanField(
        db_column="IsFeatured", blank=True, null=True
    )  # Field name made lowercase.
    autoselect = models.SmallIntegerField(
        db_column="AutoSelect", blank=True, null=True
    )  # Field name made lowercase.
    autodownload = models.SmallIntegerField(
        db_column="AutoDownload", blank=True, null=True
    )  # Field name made lowercase.
    supersedencebehavior = models.SmallIntegerField(
        db_column="SupersedenceBehavior", blank=True, null=True
    )  # Field name made lowercase.
    ispartofset = models.BooleanField(
        db_column="IsPartOfSet"
    )  # Field name made lowercase.
    setcreationtime = models.DateTimeField(
        db_column="SetCreationTime", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbDeployment"
        unique_together = (
            (
                "revisionid",
                "targetgroupid",
                "actionid",
                "targetgrouptypeid",
                "priority",
                "deploymenttime",
            ),
        )


class Tbdistributioncomputerhardwareid(models.Model):
    distributioncomputerhardwareid = models.CharField(
        db_column="DistributionComputerHardwareId", primary_key=True, max_length=36
    )  # Field name made lowercase.
    revisionid = models.ForeignKey(
        "Tbdriver", models.DO_NOTHING, db_column="RevisionID"
    )  # Field name made lowercase.
    hardwareid = models.ForeignKey(
        "Tbdriver", models.DO_NOTHING, db_column="HardwareID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbDistributionComputerHardwareId"
        unique_together = (
            ("distributioncomputerhardwareid", "revisionid", "hardwareid"),
        )


class Tbdownstreamserverclientactivityrollup(models.Model):
    clientsummaryid = models.OneToOneField(
        "Tbdownstreamserverclientsummaryrollup",
        models.DO_NOTHING,
        db_column="ClientSummaryID",
        primary_key=True,
    )  # Field name made lowercase.
    updateid = models.CharField(
        db_column="UpdateID", max_length=36
    )  # Field name made lowercase.
    revisionnumber = models.IntegerField(
        db_column="RevisionNumber"
    )  # Field name made lowercase.
    installsuccesscount = models.IntegerField(
        db_column="InstallSuccessCount"
    )  # Field name made lowercase.
    installfailurecount = models.IntegerField(
        db_column="InstallFailureCount"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbDownstreamServerClientActivityRollup"
        unique_together = (("clientsummaryid", "updateid", "revisionnumber"),)


class Tbdownstreamserverclientsummaryrollup(models.Model):
    targetid = models.ForeignKey(
        "Tbtarget", models.DO_NOTHING, db_column="TargetID", blank=True, null=True
    )  # Field name made lowercase.
    clientsummaryid = models.AutoField(
        db_column="ClientSummaryID", primary_key=True
    )  # Field name made lowercase.
    osmajorversion = models.IntegerField(
        db_column="OSMajorVersion"
    )  # Field name made lowercase.
    osminorversion = models.IntegerField(
        db_column="OSMinorVersion"
    )  # Field name made lowercase.
    osbuildnumber = models.IntegerField(
        db_column="OSBuildNumber"
    )  # Field name made lowercase.
    osservicepackmajornumber = models.IntegerField(
        db_column="OSServicePackMajorNumber"
    )  # Field name made lowercase.
    osservicepackminornumber = models.IntegerField(
        db_column="OSServicePackMinorNumber"
    )  # Field name made lowercase.
    oslocale = models.CharField(
        db_column="OSLocale", max_length=10
    )  # Field name made lowercase.
    processorarchitecture = models.CharField(
        db_column="ProcessorArchitecture", max_length=50
    )  # Field name made lowercase.
    clientcount = models.IntegerField(
        db_column="ClientCount"
    )  # Field name made lowercase.
    suitemask = models.SmallIntegerField(
        db_column="SuiteMask"
    )  # Field name made lowercase.
    oldproducttype = models.SmallIntegerField(
        db_column="OldProductType"
    )  # Field name made lowercase.
    newproducttype = models.IntegerField(
        db_column="NewProductType"
    )  # Field name made lowercase.
    systemmetrics = models.IntegerField(
        db_column="SystemMetrics"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbDownstreamServerClientSummaryRollup"


class Tbdownstreamserverrollupconfiguration(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    latesteventtime = models.DateTimeField(
        db_column="LatestEventTime", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbDownstreamServerRollupConfiguration"


class Tbdownstreamserversummaryrollup(models.Model):
    targetid = models.OneToOneField(
        "Tbtarget", models.DO_NOTHING, db_column="TargetID", primary_key=True
    )  # Field name made lowercase.
    updatecount = models.IntegerField(
        db_column="UpdateCount"
    )  # Field name made lowercase.
    declinedupdatecount = models.IntegerField(
        db_column="DeclinedUpdateCount"
    )  # Field name made lowercase.
    approvedupdatecount = models.IntegerField(
        db_column="ApprovedUpdateCount"
    )  # Field name made lowercase.
    notapprovedupdatecount = models.IntegerField(
        db_column="NotApprovedUpdateCount"
    )  # Field name made lowercase.
    updateswithstaleupdateapprovalscount = models.IntegerField(
        db_column="UpdatesWithStaleUpdateApprovalsCount"
    )  # Field name made lowercase.
    expiredupdatecount = models.IntegerField(
        db_column="ExpiredUpdateCount"
    )  # Field name made lowercase.
    criticalupdatesnotapprovedforinstallcount = models.IntegerField(
        db_column="CriticalUpdatesNotApprovedForInstallCount"
    )  # Field name made lowercase.
    wsusupdatesnotapprovedforinstallcount = models.IntegerField(
        db_column="WsusUpdatesNotApprovedForInstallCount"
    )  # Field name made lowercase.
    updateswithclienterrorscount = models.IntegerField(
        db_column="UpdatesWithClientErrorsCount"
    )  # Field name made lowercase.
    updateswithservererrorscount = models.IntegerField(
        db_column="UpdatesWithServerErrorsCount"
    )  # Field name made lowercase.
    updatesneedingfilescount = models.IntegerField(
        db_column="UpdatesNeedingFilesCount"
    )  # Field name made lowercase.
    updatesneededbycomputerscount = models.IntegerField(
        db_column="UpdatesNeededByComputersCount"
    )  # Field name made lowercase.
    updatesuptodatecount = models.IntegerField(
        db_column="UpdatesUpToDateCount"
    )  # Field name made lowercase.
    customcomputertargetgroupcount = models.IntegerField(
        db_column="CustomComputerTargetGroupCount"
    )  # Field name made lowercase.
    computertargetcount = models.IntegerField(
        db_column="ComputerTargetCount"
    )  # Field name made lowercase.
    computertargetsneedingupdatescount = models.IntegerField(
        db_column="ComputerTargetsNeedingUpdatesCount"
    )  # Field name made lowercase.
    computertargetswithupdateerrorscount = models.IntegerField(
        db_column="ComputerTargetsWithUpdateErrorsCount"
    )  # Field name made lowercase.
    computersuptodatecount = models.IntegerField(
        db_column="ComputersUpToDateCount"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbDownstreamServerSummaryRollup"


class Tbdownstreamservertarget(models.Model):
    targetid = models.OneToOneField(
        "Tbtarget", models.DO_NOTHING, db_column="TargetID", primary_key=True
    )  # Field name made lowercase.
    sid = models.BinaryField(
        db_column="SID", blank=True, null=True
    )  # Field name made lowercase.
    accountname = models.CharField(
        db_column="AccountName", max_length=255
    )  # Field name made lowercase.
    accountserverid = models.CharField(
        db_column="AccountServerID", unique=True, max_length=36, blank=True, null=True
    )  # Field name made lowercase.
    lastsynctime = models.DateTimeField(
        db_column="LastSyncTime", blank=True, null=True
    )  # Field name made lowercase.
    rolluplastsynctime = models.DateTimeField(
        db_column="RollupLastSyncTime", blank=True, null=True
    )  # Field name made lowercase.
    lastdeploymentsynctime = models.DateTimeField(
        db_column="LastDeploymentSyncTime", blank=True, null=True
    )  # Field name made lowercase.
    parentservertargetid = models.ForeignKey(
        "self",
        models.DO_NOTHING,
        db_column="ParentServerTargetID",
        blank=True,
        null=True,
    )  # Field name made lowercase.
    lastrolluptime = models.DateTimeField(
        db_column="LastRollupTime", blank=True, null=True
    )  # Field name made lowercase.
    version = models.CharField(
        db_column="Version", max_length=32, blank=True, null=True
    )  # Field name made lowercase.
    isreplica = models.BooleanField(db_column="IsReplica")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbDownstreamServerTarget"


class Tbdriver(models.Model):
    revisionid = models.OneToOneField(
        "Tbrevision", models.DO_NOTHING, db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    hardwareid = models.CharField(
        db_column="HardwareID", max_length=200
    )  # Field name made lowercase.
    driververdate = models.DateTimeField(
        db_column="DriverVerDate"
    )  # Field name made lowercase.
    driververversion = models.BigIntegerField(
        db_column="DriverVerVersion"
    )  # Field name made lowercase.
    model = models.CharField(
        db_column="Model", max_length=256
    )  # Field name made lowercase.
    manufacturer = models.CharField(
        db_column="Manufacturer", max_length=256
    )  # Field name made lowercase.
    provider = models.CharField(
        db_column="Provider", max_length=256
    )  # Field name made lowercase.
    classid = models.ForeignKey(
        "Tbdriverclass", models.DO_NOTHING, db_column="ClassID"
    )  # Field name made lowercase.
    whqldriverid = models.BigIntegerField(
        db_column="WhqlDriverID"
    )  # Field name made lowercase.
    company = models.CharField(
        db_column="Company", max_length=256
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbDriver"
        unique_together = (("revisionid", "hardwareid"),)


class Tbdriverclass(models.Model):
    classid = models.AutoField(
        db_column="ClassID", primary_key=True
    )  # Field name made lowercase.
    class_field = models.CharField(
        db_column="Class", max_length=256
    )  # Field name made lowercase. Field renamed because it was a Python reserved word.

    class Meta:
        managed = False
        db_table = "tbDriverClass"


class Tbdriverfeaturescore(models.Model):
    operatingsystem = models.CharField(
        db_column="OperatingSystem", primary_key=True, max_length=50
    )  # Field name made lowercase.
    featurescore = models.SmallIntegerField(
        db_column="FeatureScore"
    )  # Field name made lowercase.
    revisionid = models.ForeignKey(
        Tbdriver, models.DO_NOTHING, db_column="RevisionID"
    )  # Field name made lowercase.
    hardwareid = models.ForeignKey(
        Tbdriver, models.DO_NOTHING, db_column="HardwareID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbDriverFeatureScore"
        unique_together = (
            ("operatingsystem", "featurescore", "revisionid", "hardwareid"),
        )


class Tbdrivertargetinggroup(models.Model):
    drivertargetingid = models.IntegerField(
        db_column="DriverTargetingID", primary_key=True
    )  # Field name made lowercase.
    targetgroupid = models.ForeignKey(
        "Tbtargetgroup", models.DO_NOTHING, db_column="TargetGroupID"
    )  # Field name made lowercase.
    computerhardwareid = models.CharField(
        db_column="ComputerHardwareID", max_length=36
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbDriverTargetingGroup"
        unique_together = (("targetgroupid", "computerhardwareid"),)


class Tbdrivertargetinggroupprerequisite(models.Model):
    drivertargetingid = models.OneToOneField(
        Tbdrivertargetinggroup,
        models.DO_NOTHING,
        db_column="DriverTargetingID",
        primary_key=True,
    )  # Field name made lowercase.
    localupdateid = models.ForeignKey(
        "Tbupdate", models.DO_NOTHING, db_column="LocalUpdateID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbDriverTargetingGroupPrerequisite"
        unique_together = (("drivertargetingid", "localupdateid"),)


class Tbdynamiccategory(models.Model):
    id = models.CharField(
        db_column="Id", primary_key=True, max_length=36
    )  # Field name made lowercase.
    name = models.CharField(
        db_column="Name", max_length=200
    )  # Field name made lowercase.
    type = models.SmallIntegerField(db_column="Type")  # Field name made lowercase.
    enabled = models.BooleanField(db_column="Enabled")  # Field name made lowercase.
    origin = models.SmallIntegerField(db_column="Origin")  # Field name made lowercase.
    discoverytime = models.DateTimeField(
        db_column="DiscoveryTime"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbDynamicCategory"
        unique_together = (("name", "type"),)


class Tbemailnotificationrecipient(models.Model):
    emailaddress = models.CharField(
        db_column="EmailAddress", max_length=256
    )  # Field name made lowercase.
    notificationtype = models.IntegerField(
        db_column="NotificationType"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbEmailNotificationRecipient"


class Tbeulaacceptance(models.Model):
    eulaid = models.CharField(
        db_column="EulaID", primary_key=True, max_length=36
    )  # Field name made lowercase.
    accepteddate = models.DateTimeField(
        db_column="AcceptedDate"
    )  # Field name made lowercase.
    adminname = models.CharField(
        db_column="AdminName", max_length=385
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbEulaAcceptance"


class Tbeulaproperty(models.Model):
    eulafiledigest = models.ForeignKey(
        "Tbfile", models.DO_NOTHING, db_column="EulaFileDigest"
    )  # Field name made lowercase.
    revisionid = models.OneToOneField(
        "Tbrevision", models.DO_NOTHING, db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    languageid = models.ForeignKey(
        "Tblanguage", models.DO_NOTHING, db_column="LanguageID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbEulaProperty"
        unique_together = (("revisionid", "eulafiledigest", "languageid"),)


class Tbevent(models.Model):
    eventid = models.SmallIntegerField(
        db_column="EventID", primary_key=True
    )  # Field name made lowercase.
    eventnamespaceid = models.ForeignKey(
        "Tbeventnamespace", models.DO_NOTHING, db_column="EventNamespaceID"
    )  # Field name made lowercase.
    stateid = models.IntegerField(db_column="StateID")  # Field name made lowercase.
    severityid = models.IntegerField(
        db_column="SeverityID"
    )  # Field name made lowercase.
    loglevel = models.SmallIntegerField(
        db_column="LogLevel"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbEvent"
        unique_together = (("eventid", "eventnamespaceid"),)


class Tbeventinstance(models.Model):
    eventinstanceid = models.CharField(
        db_column="EventInstanceID", unique=True, max_length=36
    )  # Field name made lowercase.
    eventid = models.ForeignKey(
        Tbevent, models.DO_NOTHING, db_column="EventID"
    )  # Field name made lowercase.
    eventnamespaceid = models.ForeignKey(
        "Tbeventsource", models.DO_NOTHING, db_column="EventNamespaceID"
    )  # Field name made lowercase.
    eventsourceid = models.ForeignKey(
        "Tbeventsource", models.DO_NOTHING, db_column="EventSourceID"
    )  # Field name made lowercase.
    timeattarget = models.DateTimeField(
        db_column="TimeAtTarget"
    )  # Field name made lowercase.
    timeatserver = models.DateTimeField(
        db_column="TimeAtServer", blank=True, null=True
    )  # Field name made lowercase.
    win32hresult = models.IntegerField(
        db_column="Win32HResult"
    )  # Field name made lowercase.
    appname = models.CharField(
        db_column="AppName", max_length=256, blank=True, null=True
    )  # Field name made lowercase.
    miscdata = models.TextField(
        db_column="MiscData", blank=True, null=True
    )  # Field name made lowercase.
    replacementstrings = models.TextField(
        db_column="ReplacementStrings", blank=True, null=True
    )  # Field name made lowercase.
    computerid = models.CharField(
        db_column="ComputerID", max_length=256, blank=True, null=True
    )  # Field name made lowercase.
    updateid = models.CharField(
        db_column="UpdateID", max_length=36, blank=True, null=True
    )  # Field name made lowercase.
    revisionnumber = models.IntegerField(
        db_column="RevisionNumber", blank=True, null=True
    )  # Field name made lowercase.
    deviceid = models.TextField(
        db_column="DeviceID", blank=True, null=True
    )  # Field name made lowercase.
    eventordinalnumber = models.BigAutoField(
        db_column="EventOrdinalNumber", primary_key=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbEventInstance"


class Tbeventmessagetemplate(models.Model):
    eventid = models.OneToOneField(
        Tbevent, models.DO_NOTHING, db_column="EventID", primary_key=True
    )  # Field name made lowercase.
    eventnamespaceid = models.ForeignKey(
        Tbevent, models.DO_NOTHING, db_column="EventNamespaceID"
    )  # Field name made lowercase.
    shortlanguage = models.CharField(
        db_column="ShortLanguage", max_length=16, blank=True, null=True
    )  # Field name made lowercase.
    messagetemplate = models.CharField(
        db_column="MessageTemplate", max_length=2048
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbEventMessageTemplate"
        unique_together = (("eventid", "eventnamespaceid"),)


class Tbeventnamespace(models.Model):
    eventnamespaceid = models.IntegerField(
        db_column="EventNamespaceID", primary_key=True
    )  # Field name made lowercase.
    displaynamestring = models.CharField(
        db_column="DisplayNameString", max_length=256
    )  # Field name made lowercase.
    majorversion = models.IntegerField(
        db_column="MajorVersion"
    )  # Field name made lowercase.
    minorversion = models.IntegerField(
        db_column="MinorVersion"
    )  # Field name made lowercase.
    buildnumber = models.IntegerField(
        db_column="BuildNumber"
    )  # Field name made lowercase.
    qfenumber = models.IntegerField(db_column="QfeNumber")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbEventNamespace"


class Tbeventrollupcounters(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    latestrolluptime = models.DateTimeField(
        db_column="LatestRollupTime", blank=True, null=True
    )  # Field name made lowercase.
    latestrollupcount = models.IntegerField(
        db_column="LatestRollupCount", blank=True, null=True
    )  # Field name made lowercase.
    latestrollupstate = models.SmallIntegerField(
        db_column="LatestRollupState", blank=True, null=True
    )  # Field name made lowercase.
    currentrolluptotal = models.IntegerField(
        db_column="CurrentRollupTotal", blank=True, null=True
    )  # Field name made lowercase.
    currentrollupdone = models.IntegerField(
        db_column="CurrentRollupDone", blank=True, null=True
    )  # Field name made lowercase.
    currentrollupstate = models.SmallIntegerField(
        db_column="CurrentRollupState", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbEventRollupCounters"


class Tbeventsource(models.Model):
    eventsourceid = models.SmallIntegerField(
        db_column="EventSourceID", primary_key=True
    )  # Field name made lowercase.
    eventnamespaceid = models.ForeignKey(
        Tbeventnamespace, models.DO_NOTHING, db_column="EventNamespaceID"
    )  # Field name made lowercase.
    displaynamestring = models.CharField(
        db_column="DisplayNameString", max_length=256
    )  # Field name made lowercase.
    majorversion = models.IntegerField(
        db_column="MajorVersion"
    )  # Field name made lowercase.
    minorversion = models.IntegerField(
        db_column="MinorVersion"
    )  # Field name made lowercase.
    buildnumber = models.IntegerField(
        db_column="BuildNumber"
    )  # Field name made lowercase.
    qfenumber = models.IntegerField(db_column="QfeNumber")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbEventSource"
        unique_together = (("eventsourceid", "eventnamespaceid"),)


class Tbexpandedtargetintargetgroup(models.Model):
    targetgroupid = models.OneToOneField(
        "Tbtargetgroup", models.DO_NOTHING, db_column="TargetGroupID", primary_key=True
    )  # Field name made lowercase.
    targetid = models.ForeignKey(
        "Tbtarget", models.DO_NOTHING, db_column="TargetID"
    )  # Field name made lowercase.
    isexplicitmember = models.BooleanField(
        db_column="IsExplicitMember"
    )  # Field name made lowercase.
    childgroupreferences = models.IntegerField(
        db_column="ChildGroupReferences"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbExpandedTargetInTargetGroup"
        unique_together = (("targetgroupid", "targetid"),)


class Tbfile(models.Model):
    filedigest = models.TextField(
        db_column="FileDigest", primary_key=True
    )  # Field name made lowercase. This field type is a guess.
    filename = models.CharField(
        db_column="FileName", max_length=256
    )  # Field name made lowercase.
    modified = models.DateTimeField(
        db_column="Modified", blank=True, null=True
    )  # Field name made lowercase.
    size = models.BigIntegerField(db_column="Size")  # Field name made lowercase.
    iseula = models.BooleanField(db_column="IsEula")  # Field name made lowercase.
    muurl = models.CharField(
        db_column="MUURL", max_length=1024, blank=True, null=True
    )  # Field name made lowercase.
    ussurl = models.CharField(
        db_column="USSURL", max_length=1024, blank=True, null=True
    )  # Field name made lowercase.
    isexternalcab = models.BooleanField(
        db_column="IsExternalCab"
    )  # Field name made lowercase.
    issecure = models.BooleanField(db_column="IsSecure")  # Field name made lowercase.
    isencrypted = models.BooleanField(
        db_column="IsEncrypted"
    )  # Field name made lowercase.
    decryptionkey = models.BinaryField(
        db_column="DecryptionKey", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbFile"


class Tbfiledownloadprogress(models.Model):
    id = models.BigAutoField(db_column="ID")  # Field name made lowercase.
    rowid = models.CharField(
        db_column="RowID", primary_key=True, max_length=36
    )  # Field name made lowercase.
    totalbytesfordownload = models.BigIntegerField(
        db_column="TotalBytesForDownload"
    )  # Field name made lowercase.
    bytesdownloaded = models.BigIntegerField(
        db_column="BytesDownloaded"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbFileDownloadProgress"


class Tbfileforrevision(models.Model):
    revisionid = models.OneToOneField(
        "Tbrevision", models.DO_NOTHING, db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    filedigest = models.ForeignKey(
        Tbfile, models.DO_NOTHING, db_column="FileDigest"
    )  # Field name made lowercase.
    patchingtype = models.SmallIntegerField(
        db_column="PatchingType"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbFileForRevision"
        unique_together = (("revisionid", "filedigest"),)


class Tbfilehash(models.Model):
    filedigest = models.TextField(
        db_column="FileDigest", primary_key=True
    )  # Field name made lowercase. This field type is a guess.
    digestalgorithm = models.CharField(
        db_column="DigestAlgorithm", max_length=64
    )  # Field name made lowercase.
    additionalhash = models.BinaryField(
        db_column="AdditionalHash"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbFileHash"


class Tbfileonserver(models.Model):
    filedigest = models.OneToOneField(
        Tbfile, models.DO_NOTHING, db_column="FileDigest", primary_key=True
    )  # Field name made lowercase.
    configurationid = models.IntegerField(
        db_column="ConfigurationID"
    )  # Field name made lowercase.
    rowid = models.CharField(
        db_column="RowID", max_length=36
    )  # Field name made lowercase.
    desiredstate = models.IntegerField(
        db_column="DesiredState"
    )  # Field name made lowercase.
    actualstate = models.IntegerField(
        db_column="ActualState"
    )  # Field name made lowercase.
    timeaddedtoqueue = models.DateTimeField(
        db_column="TimeAddedToQueue", blank=True, null=True
    )  # Field name made lowercase.
    dssrequesteddownload = models.BooleanField(
        db_column="DSSRequestedDownload"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbFileOnServer"
        unique_together = (("filedigest", "configurationid"),)


class Tbflattenedrevisionincategory(models.Model):
    revisionid = models.OneToOneField(
        "Tbrevision", models.DO_NOTHING, db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    categoryid = models.ForeignKey(
        Tbcategory, models.DO_NOTHING, db_column="CategoryID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbFlattenedRevisionInCategory"
        unique_together = (("revisionid", "categoryid"),)


class Tbflattenedtargetgroup(models.Model):
    targetgroupid = models.OneToOneField(
        "Tbtargetgroup", models.DO_NOTHING, db_column="TargetGroupID", primary_key=True
    )  # Field name made lowercase.
    parentgroupid = models.ForeignKey(
        "Tbtargetgroup", models.DO_NOTHING, db_column="ParentGroupID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbFlattenedTargetGroup"
        unique_together = (("targetgroupid", "parentgroupid"),)


class Tbfrontendservershealth(models.Model):
    servername = models.CharField(
        db_column="ServerName", max_length=1024
    )  # Field name made lowercase.
    componentname = models.CharField(
        db_column="ComponentName", max_length=256
    )  # Field name made lowercase.
    heartbeat = models.DateTimeField(
        db_column="HeartBeat"
    )  # Field name made lowercase.
    isrunning = models.BooleanField(db_column="IsRunning")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbFrontEndServersHealth"


class Tbgroupauthorization(models.Model):
    pluginid = models.OneToOneField(
        Tbauthorization, models.DO_NOTHING, db_column="PluginID", primary_key=True
    )  # Field name made lowercase.
    groupid = models.CharField(
        db_column="GroupID", max_length=36
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbGroupAuthorization"
        unique_together = (("pluginid", "groupid"),)


class Tbhandler(models.Model):
    handlerid = models.AutoField(
        db_column="HandlerID", primary_key=True
    )  # Field name made lowercase.
    name = models.CharField(
        db_column="Name", max_length=256
    )  # Field name made lowercase.
    schemauri = models.CharField(
        db_column="SchemaURI", max_length=256
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbHandler"


class Tbimplicitcategory(models.Model):
    categoryid = models.OneToOneField(
        Tbcategory, models.DO_NOTHING, db_column="CategoryID", primary_key=True
    )  # Field name made lowercase.
    subscriptionid = models.IntegerField(
        db_column="SubscriptionID"
    )  # Field name made lowercase.
    deltasync = models.BooleanField(db_column="DeltaSync")  # Field name made lowercase.
    categorytype = models.CharField(
        db_column="CategoryType", max_length=256
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbImplicitCategory"
        unique_together = (("categoryid", "subscriptionid"),)


class Tbinstalledupdatesufficientforprerequisite(models.Model):
    prerequisiteid = models.ForeignKey(
        "Tbprerequisite", models.DO_NOTHING, db_column="PrerequisiteID"
    )  # Field name made lowercase.
    localupdateid = models.OneToOneField(
        "Tbupdate", models.DO_NOTHING, db_column="LocalUpdateID", primary_key=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbInstalledUpdateSufficientForPrerequisite"
        unique_together = (("localupdateid", "prerequisiteid"),)


class Tbinventoryclass(models.Model):
    classid = models.AutoField(
        db_column="ClassID", primary_key=True
    )  # Field name made lowercase.
    name = models.CharField(
        db_column="Name", unique=True, max_length=256
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbInventoryClass"


class Tbinventoryclassinstance(models.Model):
    classinstanceid = models.AutoField(
        db_column="ClassInstanceID", primary_key=True
    )  # Field name made lowercase.
    classid = models.ForeignKey(
        Tbinventoryclass, models.DO_NOTHING, db_column="ClassID"
    )  # Field name made lowercase.
    targetid = models.ForeignKey(
        Tbcomputertarget, models.DO_NOTHING, db_column="TargetID"
    )  # Field name made lowercase.
    keyvalue = models.CharField(
        db_column="KeyValue", max_length=256, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbInventoryClassInstance"
        unique_together = (("targetid", "classid", "keyvalue"),)


class Tbinventoryproperty(models.Model):
    propertyid = models.AutoField(
        db_column="PropertyID", primary_key=True
    )  # Field name made lowercase.
    classid = models.ForeignKey(
        Tbinventoryclass, models.DO_NOTHING, db_column="ClassID"
    )  # Field name made lowercase.
    name = models.CharField(
        db_column="Name", max_length=256
    )  # Field name made lowercase.
    type = models.CharField(
        db_column="Type", max_length=10
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbInventoryProperty"
        unique_together = (("classid", "name"),)


class Tbinventorypropertyinstance(models.Model):
    classinstanceid = models.OneToOneField(
        Tbinventoryclassinstance,
        models.DO_NOTHING,
        db_column="ClassInstanceID",
        primary_key=True,
    )  # Field name made lowercase.
    propertyid = models.ForeignKey(
        Tbinventoryproperty, models.DO_NOTHING, db_column="PropertyID"
    )  # Field name made lowercase.
    value = models.CharField(
        db_column="Value", max_length=256, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbInventoryPropertyInstance"
        unique_together = (("classinstanceid", "propertyid"),)


class Tbinventoryrule(models.Model):
    ruleid = models.CharField(
        db_column="RuleID", primary_key=True, max_length=36
    )  # Field name made lowercase.
    version = models.CharField(
        db_column="Version", max_length=20
    )  # Field name made lowercase.
    rulexml = models.TextField(
        db_column="RuleXml"
    )  # Field name made lowercase. This field type is a guess.

    class Meta:
        managed = False
        db_table = "tbInventoryRule"


class Tbinventoryxml(models.Model):
    targetid = models.OneToOneField(
        Tbcomputertarget, models.DO_NOTHING, db_column="TargetID", primary_key=True
    )  # Field name made lowercase.
    compressedxml = models.BinaryField(
        db_column="CompressedXml", blank=True, null=True
    )  # Field name made lowercase.
    rawxml = models.TextField(
        db_column="RawXml", blank=True, null=True
    )  # Field name made lowercase.
    updatetime = models.DateTimeField(
        db_column="UpdateTime"
    )  # Field name made lowercase.
    isprocessed = models.BooleanField(
        db_column="IsProcessed"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbInventoryXml"


class Tbkbarticleforrevision(models.Model):
    revisionid = models.OneToOneField(
        "Tbrevision", models.DO_NOTHING, db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    kbarticleid = models.CharField(
        db_column="KBArticleID", max_length=15
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbKBArticleForRevision"
        unique_together = (("revisionid", "kbarticleid"),)


class Tblanguage(models.Model):
    languageindex = models.AutoField(
        db_column="LanguageIndex"
    )  # Field name made lowercase.
    languageid = models.IntegerField(
        db_column="LanguageID", primary_key=True
    )  # Field name made lowercase.
    shortlanguage = models.CharField(
        db_column="ShortLanguage", max_length=16
    )  # Field name made lowercase.
    longlanguage = models.CharField(
        db_column="LongLanguage", max_length=32
    )  # Field name made lowercase.
    ussenabled = models.BooleanField(
        db_column="UssEnabled"
    )  # Field name made lowercase.
    enabled = models.BooleanField(db_column="Enabled")  # Field name made lowercase.
    createtime = models.DateTimeField(
        db_column="CreateTime"
    )  # Field name made lowercase.
    languageanchor = models.BigIntegerField(
        db_column="LanguageAnchor"
    )  # Field name made lowercase.
    fulltextlcid = models.IntegerField(
        db_column="FullTextLCID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbLanguage"


class Tblanguageinsubscription(models.Model):
    subscriptionid = models.IntegerField(
        db_column="SubscriptionID"
    )  # Field name made lowercase.
    languageid = models.OneToOneField(
        Tblanguage, models.DO_NOTHING, db_column="LanguageID", primary_key=True
    )  # Field name made lowercase.
    deltasync = models.BooleanField(db_column="DeltaSync")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbLanguageInSubscription"
        unique_together = (("languageid", "subscriptionid"),)


class Tblocalemap(models.Model):
    localeid = models.SmallIntegerField(
        db_column="LocaleId", primary_key=True
    )  # Field name made lowercase.
    lcid = models.IntegerField(db_column="LCID")  # Field name made lowercase.
    localename = models.CharField(
        db_column="LocaleName", max_length=10, blank=True, null=True
    )  # Field name made lowercase.
    localelongname = models.CharField(
        db_column="LocaleLongName", max_length=50, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbLocaleMap"


class Tblocalizedproperty(models.Model):
    localizedpropertyid = models.AutoField(
        db_column="LocalizedPropertyID", primary_key=True
    )  # Field name made lowercase.
    title = models.CharField(
        db_column="Title", max_length=200
    )  # Field name made lowercase.
    description = models.CharField(
        db_column="Description", max_length=1500, blank=True, null=True
    )  # Field name made lowercase.
    releasenote = models.CharField(
        db_column="ReleaseNote", max_length=1000, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbLocalizedProperty"


class Tblocalizedpropertyforrevision(models.Model):
    revisionid = models.OneToOneField(
        "Tbrevision", models.DO_NOTHING, db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    localizedpropertyid = models.ForeignKey(
        Tblocalizedproperty, models.DO_NOTHING, db_column="LocalizedPropertyID"
    )  # Field name made lowercase.
    languageid = models.ForeignKey(
        Tblanguage, models.DO_NOTHING, db_column="LanguageID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbLocalizedPropertyForRevision"
        unique_together = (("revisionid", "languageid", "localizedpropertyid"),)


class TbmappingTargetDynamiccategory(models.Model):
    targetid = models.ForeignKey(
        "Tbtarget", models.DO_NOTHING, db_column="TargetId"
    )  # Field name made lowercase.
    dynamiccategoryid = models.OneToOneField(
        Tbdynamiccategory,
        models.DO_NOTHING,
        db_column="DynamicCategoryId",
        primary_key=True,
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbMapping_Target_DynamicCategory"
        unique_together = (("dynamiccategoryid", "targetid"),)


class Tbmoreinfourlforrevision(models.Model):
    revisionurlid = models.AutoField(
        db_column="RevisionURLID", primary_key=True
    )  # Field name made lowercase.
    revisionid = models.ForeignKey(
        "Tbrevision", models.DO_NOTHING, db_column="RevisionID"
    )  # Field name made lowercase.
    moreinfourl = models.CharField(
        db_column="MoreInfoURL", max_length=2083
    )  # Field name made lowercase.
    shortlanguage = models.CharField(
        db_column="ShortLanguage", max_length=16
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbMoreInfoURLForRevision"


class Tbnotificationevent(models.Model):
    notificationeventid = models.AutoField(
        db_column="NotificationEventID", primary_key=True
    )  # Field name made lowercase.
    notificationeventname = models.CharField(
        db_column="NotificationEventName", max_length=256
    )  # Field name made lowercase.
    state = models.IntegerField(db_column="State")  # Field name made lowercase.
    rowid = models.CharField(
        db_column="RowID", max_length=36
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbNotificationEvent"


class Tbosmap(models.Model):
    osid = models.SmallIntegerField(
        db_column="OSid", primary_key=True
    )  # Field name made lowercase.
    osmajorversion = models.SmallIntegerField(
        db_column="OSMajorVersion"
    )  # Field name made lowercase.
    osminorversion = models.SmallIntegerField(
        db_column="OSMinorVersion"
    )  # Field name made lowercase.
    osbuildnumber = models.SmallIntegerField(
        db_column="OSBuildNumber"
    )  # Field name made lowercase.
    osservicepackmajornumber = models.SmallIntegerField(
        db_column="OSServicePackMajorNumber"
    )  # Field name made lowercase.
    osservicepackminornumber = models.SmallIntegerField(
        db_column="OSServicePackMinorNumber"
    )  # Field name made lowercase.
    processorarchitecture = models.CharField(
        db_column="ProcessorArchitecture", max_length=50
    )  # Field name made lowercase.
    osshortname = models.CharField(
        db_column="OSShortName", max_length=16, blank=True, null=True
    )  # Field name made lowercase.
    oslongname = models.CharField(
        db_column="OSLongName", max_length=256, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbOSMap"


class Tbprecomputedlocalizedproperty(models.Model):
    precomputedlocalizedpropertyid = models.AutoField(
        db_column="PreComputedLocalizedPropertyID", primary_key=True
    )  # Field name made lowercase.
    shortlanguage = models.CharField(
        db_column="ShortLanguage", max_length=16
    )  # Field name made lowercase.
    title = models.CharField(
        db_column="Title", max_length=200
    )  # Field name made lowercase.
    description = models.CharField(
        db_column="Description", max_length=1500, blank=True, null=True
    )  # Field name made lowercase.
    releasenotes = models.CharField(
        db_column="ReleaseNotes", max_length=1000, blank=True, null=True
    )  # Field name made lowercase.
    updateid = models.CharField(
        db_column="UpdateID", max_length=36, blank=True, null=True
    )  # Field name made lowercase.
    revisionnumber = models.IntegerField(
        db_column="RevisionNumber", blank=True, null=True
    )  # Field name made lowercase.
    revisionid = models.ForeignKey(
        "Tbrevision", models.DO_NOTHING, db_column="RevisionID", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbPreComputedLocalizedProperty"


class Tbprecomputedcategorylocalizedproperty(models.Model):
    categoryid = models.OneToOneField(
        Tbcategory, models.DO_NOTHING, db_column="CategoryID", primary_key=True
    )  # Field name made lowercase.
    shortlanguage = models.CharField(
        db_column="ShortLanguage", max_length=16
    )  # Field name made lowercase.
    title = models.CharField(
        db_column="Title", max_length=200
    )  # Field name made lowercase.
    categorytype = models.ForeignKey(
        Tbcategorytype, models.DO_NOTHING, db_column="CategoryType"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbPrecomputedCategoryLocalizedProperty"
        unique_together = (("categoryid", "shortlanguage"),)


class Tbprerequisite(models.Model):
    prerequisiteid = models.AutoField(
        db_column="PrerequisiteID", primary_key=True
    )  # Field name made lowercase.
    revisionid = models.ForeignKey(
        "Tbrevision", models.DO_NOTHING, db_column="RevisionID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbPrerequisite"


class Tbprerequisitedependency(models.Model):
    revisionid = models.IntegerField(
        db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    prerequisitelocalupdateid = models.IntegerField(
        db_column="PrerequisiteLocalUpdateID"
    )  # Field name made lowercase.
    prerequisiterevisionid = models.IntegerField(
        db_column="PrerequisiteRevisionID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbPrerequisiteDependency"
        unique_together = (("revisionid", "prerequisiterevisionid"),)


class Tbprogramkeys(models.Model):
    programkey = models.CharField(
        db_column="ProgramKey", primary_key=True, max_length=36
    )  # Field name made lowercase.
    description = models.CharField(
        db_column="Description", max_length=256, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbProgramKeys"


class Tbproperty(models.Model):
    revisionid = models.OneToOneField(
        "Tbrevision", models.DO_NOTHING, db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    publicationstate = models.IntegerField(
        db_column="PublicationState"
    )  # Field name made lowercase.
    creationdate = models.DateTimeField(
        db_column="CreationDate"
    )  # Field name made lowercase.
    receivedfromcreatorservice = models.DateTimeField(
        db_column="ReceivedFromCreatorService"
    )  # Field name made lowercase.
    explicitlydeployable = models.BooleanField(
        db_column="ExplicitlyDeployable"
    )  # Field name made lowercase.
    caninstall = models.BooleanField(
        db_column="CanInstall", blank=True, null=True
    )  # Field name made lowercase.
    installationimpact = models.IntegerField(
        db_column="InstallationImpact", blank=True, null=True
    )  # Field name made lowercase.
    installrequiresconnectivity = models.BooleanField(
        db_column="InstallRequiresConnectivity", blank=True, null=True
    )  # Field name made lowercase.
    installrequiresuserinput = models.BooleanField(
        db_column="InstallRequiresUserInput", blank=True, null=True
    )  # Field name made lowercase.
    installrebootbehavior = models.IntegerField(
        db_column="InstallRebootBehavior", blank=True, null=True
    )  # Field name made lowercase.
    canuninstall = models.BooleanField(
        db_column="CanUninstall", blank=True, null=True
    )  # Field name made lowercase.
    uninstallimpact = models.IntegerField(
        db_column="UninstallImpact", blank=True, null=True
    )  # Field name made lowercase.
    uninstallrequiresconnectivity = models.BooleanField(
        db_column="UninstallRequiresConnectivity", blank=True, null=True
    )  # Field name made lowercase.
    uninstallrequiresuserinput = models.BooleanField(
        db_column="UninstallRequiresUserInput", blank=True, null=True
    )  # Field name made lowercase.
    uninstallrebootbehavior = models.IntegerField(
        db_column="UninstallRebootBehavior", blank=True, null=True
    )  # Field name made lowercase.
    handlerid = models.IntegerField(
        db_column="HandlerID", blank=True, null=True
    )  # Field name made lowercase.
    eulaid = models.CharField(
        db_column="EulaID", max_length=36, blank=True, null=True
    )  # Field name made lowercase.
    requiresreacceptanceofeula = models.BooleanField(
        db_column="RequiresReacceptanceOfEula", blank=True, null=True
    )  # Field name made lowercase.
    defaultpropertieslanguageid = models.IntegerField(
        db_column="DefaultPropertiesLanguageID", blank=True, null=True
    )  # Field name made lowercase.
    updatetype = models.CharField(
        db_column="UpdateType", max_length=256
    )  # Field name made lowercase.
    eulaexplicitlyaccepted = models.BooleanField(
        db_column="EulaExplicitlyAccepted"
    )  # Field name made lowercase.
    msrcseverity = models.CharField(
        db_column="MsrcSeverity", max_length=20
    )  # Field name made lowercase.
    compatibleprotocolversion = models.CharField(
        db_column="CompatibleProtocolVersion", max_length=20, blank=True, null=True
    )  # Field name made lowercase.
    oemonlydriver = models.BooleanField(
        db_column="OemOnlyDriver", blank=True, null=True
    )  # Field name made lowercase.
    drivermininstalledversion = models.BigIntegerField(
        db_column="DriverMinInstalledVersion", blank=True, null=True
    )  # Field name made lowercase.
    drivermaxinstalledversion = models.BigIntegerField(
        db_column="DriverMaxInstalledVersion", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbProperty"


class Tbreference(models.Model):
    wusfrontendservername = models.CharField(
        db_column="WUSFrontEndServerName", max_length=1024
    )  # Field name made lowercase.
    wusfrontendserverport = models.IntegerField(
        db_column="WUSFrontEndServerPort"
    )  # Field name made lowercase.
    nlbmasterfrontendserver = models.CharField(
        db_column="NLBMasterFrontEndServer", max_length=1024
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbReference"


class Tbrequestedtargetgroup(models.Model):
    requestedtargetgroupid = models.AutoField(
        db_column="RequestedTargetGroupID", primary_key=True
    )  # Field name made lowercase.
    name = models.CharField(
        db_column="Name", unique=True, max_length=256
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbRequestedTargetGroup"


class Tbrequestedtargetgroupsfortarget(models.Model):
    targetid = models.OneToOneField(
        "Tbtarget", models.DO_NOTHING, db_column="TargetID", primary_key=True
    )  # Field name made lowercase.
    requestedtargetgroupid = models.ForeignKey(
        Tbrequestedtargetgroup, models.DO_NOTHING, db_column="RequestedTargetGroupID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbRequestedTargetGroupsForTarget"
        unique_together = (("targetid", "requestedtargetgroupid"),)


class Tbrevision(models.Model):
    revisionid = models.AutoField(
        db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    localupdateid = models.ForeignKey(
        "Tbupdate", models.DO_NOTHING, db_column="LocalUpdateID"
    )  # Field name made lowercase.
    revisionnumber = models.IntegerField(
        db_column="RevisionNumber"
    )  # Field name made lowercase.
    lastisleafchange = models.DateTimeField(
        db_column="LastIsLeafChange", blank=True, null=True
    )  # Field name made lowercase.
    isleaf = models.BooleanField(db_column="IsLeaf")  # Field name made lowercase.
    isbeta = models.BooleanField(db_column="IsBeta")  # Field name made lowercase.
    timetogoliveoncatalog = models.DateTimeField(
        db_column="TimeToGoLiveOnCatalog", blank=True, null=True
    )  # Field name made lowercase.
    rowid = models.CharField(
        db_column="RowID", unique=True, max_length=36
    )  # Field name made lowercase.
    state = models.SmallIntegerField(db_column="State")  # Field name made lowercase.
    origin = models.IntegerField(db_column="Origin")  # Field name made lowercase.
    iscritical = models.BooleanField(
        db_column="IsCritical"
    )  # Field name made lowercase.
    languagemask = models.BigIntegerField(
        db_column="LanguageMask"
    )  # Field name made lowercase.
    islatestrevision = models.BooleanField(
        db_column="IsLatestRevision"
    )  # Field name made lowercase.
    ismandatory = models.BooleanField(
        db_column="IsMandatory"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbRevision"


class Tbrevisionextendedlanguagemask(models.Model):
    revisionid = models.OneToOneField(
        Tbrevision, models.DO_NOTHING, db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    languagemask2 = models.BigIntegerField(
        db_column="LanguageMask2"
    )  # Field name made lowercase.
    languagemask3 = models.BigIntegerField(
        db_column="LanguageMask3"
    )  # Field name made lowercase.
    languagemask4 = models.BigIntegerField(
        db_column="LanguageMask4"
    )  # Field name made lowercase.
    languagemask5 = models.BigIntegerField(
        db_column="LanguageMask5"
    )  # Field name made lowercase.
    languagemask6 = models.BigIntegerField(
        db_column="LanguageMask6"
    )  # Field name made lowercase.
    languagemask7 = models.BigIntegerField(
        db_column="LanguageMask7"
    )  # Field name made lowercase.
    languagemask8 = models.BigIntegerField(
        db_column="LanguageMask8"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbRevisionExtendedLanguageMask"


class Tbrevisionextendedproperty(models.Model):
    revisionid = models.OneToOneField(
        Tbrevision, models.DO_NOTHING, db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    recommendedmemory = models.BigIntegerField(
        db_column="RecommendedMemory", blank=True, null=True
    )  # Field name made lowercase.
    recommendedharddiskspace = models.BigIntegerField(
        db_column="RecommendedHardDiskSpace", blank=True, null=True
    )  # Field name made lowercase.
    prerequisitesxml = models.TextField(
        db_column="PrerequisitesXml", blank=True, null=True
    )  # Field name made lowercase.
    isinstalledxml = models.TextField(
        db_column="IsInstalledXml", blank=True, null=True
    )  # Field name made lowercase.
    isinstallablexml = models.TextField(
        db_column="IsInstallableXml", blank=True, null=True
    )  # Field name made lowercase.
    handlerspecificdataxml = models.TextField(
        db_column="HandlerSpecificDataXml", blank=True, null=True
    )  # Field name made lowercase.
    extendedapplicabilityxml = models.TextField(
        db_column="ExtendedApplicabilityXml", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbRevisionExtendedProperty"


class Tbrevisionincategory(models.Model):
    revisionid = models.OneToOneField(
        Tbrevision, models.DO_NOTHING, db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    categoryid = models.ForeignKey(
        Tbcategory, models.DO_NOTHING, db_column="CategoryID"
    )  # Field name made lowercase.
    expanded = models.BooleanField(db_column="Expanded")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbRevisionInCategory"
        unique_together = (("revisionid", "categoryid"),)


class Tbrevisionlanguage(models.Model):
    languageid = models.ForeignKey(
        Tblanguage, models.DO_NOTHING, db_column="LanguageID"
    )  # Field name made lowercase.
    revisionid = models.OneToOneField(
        Tbrevision, models.DO_NOTHING, db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    expanded = models.BooleanField(db_column="Expanded")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbRevisionLanguage"
        unique_together = (("revisionid", "languageid"),)


class Tbrevisionsupersedesupdate(models.Model):
    revisionid = models.OneToOneField(
        Tbrevision, models.DO_NOTHING, db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    supersededupdateid = models.CharField(
        db_column="SupersededUpdateID", max_length=36
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbRevisionSupersedesUpdate"
        unique_together = (("revisionid", "supersededupdateid"),)


class Tbschedule(models.Model):
    scheduletarget = models.IntegerField(
        db_column="ScheduleTarget", primary_key=True
    )  # Field name made lowercase.
    scheduleid = models.IntegerField(
        db_column="ScheduleID"
    )  # Field name made lowercase.
    lastmodifiedby = models.CharField(
        db_column="LastModifiedBy", max_length=385
    )  # Field name made lowercase.
    lastmodifiedtime = models.DateTimeField(
        db_column="LastModifiedTime"
    )  # Field name made lowercase.
    scheduledtimeofday = models.IntegerField(
        db_column="ScheduledTimeOfDay", blank=True, null=True
    )  # Field name made lowercase.
    scheduledruntime = models.DateTimeField(
        db_column="ScheduledRunTime", blank=True, null=True
    )  # Field name made lowercase.
    lastruntime = models.DateTimeField(
        db_column="LastRunTime"
    )  # Field name made lowercase.
    frequency = models.IntegerField(
        db_column="Frequency", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbSchedule"
        unique_together = (("scheduletarget", "scheduleid"),)


class Tbschemaversion(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    componentname = models.CharField(
        db_column="ComponentName", max_length=20
    )  # Field name made lowercase.
    buildnumber = models.CharField(
        db_column="BuildNumber", max_length=11
    )  # Field name made lowercase.
    schemaversion = models.IntegerField(
        db_column="SchemaVersion"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbSchemaVersion"


class Tbschemaversionhistory(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    componentname = models.CharField(
        db_column="ComponentName", max_length=20
    )  # Field name made lowercase.
    buildnumber = models.CharField(
        db_column="BuildNumber", max_length=11
    )  # Field name made lowercase.
    schemaversion = models.IntegerField(
        db_column="SchemaVersion"
    )  # Field name made lowercase.
    archivedtime = models.DateTimeField(
        db_column="ArchivedTime"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbSchemaVersionHistory"


class Tbsecuritybulletinforrevision(models.Model):
    revisionid = models.OneToOneField(
        Tbrevision, models.DO_NOTHING, db_column="RevisionID", primary_key=True
    )  # Field name made lowercase.
    securitybulletinid = models.CharField(
        db_column="SecurityBulletinID", max_length=15
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbSecurityBulletinForRevision"
        unique_together = (("revisionid", "securitybulletinid"),)


class Tbserverhealth(models.Model):
    componentname = models.CharField(
        db_column="ComponentName", primary_key=True, max_length=256
    )  # Field name made lowercase.
    heartbeat = models.DateTimeField(
        db_column="HeartBeat"
    )  # Field name made lowercase.
    isrunning = models.BooleanField(db_column="IsRunning")  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbServerHealth"


class Tbserversyncresult(models.Model):
    languagelist = models.CharField(
        db_column="LanguageList", max_length=200
    )  # Field name made lowercase.
    categorylist = models.CharField(
        db_column="CategoryList", primary_key=True, max_length=200
    )  # Field name made lowercase.
    updateclassificationlist = models.CharField(
        db_column="UpdateClassificationList", max_length=200
    )  # Field name made lowercase.
    resultxml = models.CharField(
        db_column="ResultXml", max_length=7000, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbServerSyncResult"


class Tbsingletondata(models.Model):
    id = models.AutoField(
        db_column="ID", primary_key=True
    )  # Field name made lowercase.
    usshostonmu = models.BooleanField(
        db_column="UssHostOnMu"
    )  # Field name made lowercase.
    lastautopurgedatetime = models.DateTimeField(
        db_column="LastAutoPurgeDateTime", blank=True, null=True
    )  # Field name made lowercase.
    resetstatemachineneeded = models.BooleanField(
        db_column="ResetStateMachineNeeded"
    )  # Field name made lowercase.
    lasttimereporttomu = models.DateTimeField(
        db_column="LastTimeReportToMU"
    )  # Field name made lowercase.
    offlinesyncexclusionlistxml = models.TextField(
        db_column="OfflineSyncExclusionListXml", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbSingletonData"


class Tbstatemachine(models.Model):
    statemachineid = models.AutoField(
        db_column="StateMachineID"
    )  # Field name made lowercase.
    statemachinename = models.CharField(
        db_column="StateMachineName", primary_key=True, max_length=256
    )  # Field name made lowercase.
    selectproc = models.CharField(
        db_column="SelectProc", max_length=128
    )  # Field name made lowercase.
    updateproc = models.CharField(
        db_column="UpdateProc", max_length=128
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbStateMachine"


class Tbstatemachineevent(models.Model):
    statemachineid = models.IntegerField(
        db_column="StateMachineID", primary_key=True
    )  # Field name made lowercase.
    eventid = models.IntegerField(db_column="EventID")  # Field name made lowercase.
    eventname = models.CharField(
        db_column="EventName", max_length=256
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbStateMachineEvent"
        unique_together = (
            ("statemachineid", "eventname"),
            ("statemachineid", "eventid"),
        )


class Tbstatemachineeventtransitionlog(models.Model):
    entryid = models.AutoField(
        db_column="EntryID", primary_key=True
    )  # Field name made lowercase.
    eventtime = models.DateTimeField(
        db_column="EventTime"
    )  # Field name made lowercase.
    statemachineid = models.IntegerField(
        db_column="StateMachineID"
    )  # Field name made lowercase.
    rowid = models.CharField(
        db_column="RowID", max_length=36
    )  # Field name made lowercase.
    oldstateid = models.IntegerField(
        db_column="OldStateID"
    )  # Field name made lowercase.
    eventid = models.IntegerField(db_column="EventID")  # Field name made lowercase.
    newstateid = models.IntegerField(
        db_column="NewStateID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbStateMachineEventTransitionLog"


class Tbstatemachinestate(models.Model):
    statemachineid = models.IntegerField(
        db_column="StateMachineID", primary_key=True
    )  # Field name made lowercase.
    stateid = models.IntegerField(db_column="StateID")  # Field name made lowercase.
    statename = models.CharField(
        db_column="StateName", max_length=256
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbStateMachineState"
        unique_together = (
            ("statemachineid", "stateid"),
            ("statemachineid", "statename"),
        )


class Tbstatemachinetransition(models.Model):
    statemachineid = models.IntegerField(
        db_column="StateMachineID", primary_key=True
    )  # Field name made lowercase.
    stateid = models.IntegerField(db_column="StateID")  # Field name made lowercase.
    eventid = models.IntegerField(db_column="EventID")  # Field name made lowercase.
    newstateid = models.IntegerField(
        db_column="NewStateID"
    )  # Field name made lowercase.
    storedprocedure = models.CharField(
        db_column="StoredProcedure", max_length=256, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbStateMachineTransition"
        unique_together = (("statemachineid", "stateid", "eventid"),)


class Tbtarget(models.Model):
    targetid = models.AutoField(
        db_column="TargetID", primary_key=True
    )  # Field name made lowercase.
    targettypeid = models.ForeignKey(
        "Tbtargettype", models.DO_NOTHING, db_column="TargetTypeID"
    )  # Field name made lowercase.
    name = models.CharField(
        db_column="Name", max_length=256, blank=True, null=True
    )  # Field name made lowercase.
    isnewclient = models.BooleanField(
        db_column="IsNewClient"
    )  # Field name made lowercase.
    description = models.CharField(
        db_column="Description", max_length=1024, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbTarget"


class Tbtargetcomputerhardwareid(models.Model):
    targetcomputerhardwareid = models.CharField(
        db_column="TargetComputerHardwareId", primary_key=True, max_length=36
    )  # Field name made lowercase.
    revisionid = models.ForeignKey(
        Tbdriver, models.DO_NOTHING, db_column="RevisionID"
    )  # Field name made lowercase.
    hardwareid = models.ForeignKey(
        Tbdriver, models.DO_NOTHING, db_column="HardwareID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbTargetComputerHardwareId"
        unique_together = (("targetcomputerhardwareid", "revisionid", "hardwareid"),)


class Tbtargetgroup(models.Model):
    targetgrouptypeid = models.ForeignKey(
        "Tbtargetgrouptype", models.DO_NOTHING, db_column="TargetGroupTypeID"
    )  # Field name made lowercase.
    name = models.CharField(
        db_column="Name", max_length=256
    )  # Field name made lowercase.
    description = models.CharField(
        db_column="Description", max_length=1024, blank=True, null=True
    )  # Field name made lowercase.
    targetgroupid = models.CharField(
        db_column="TargetGroupID", primary_key=True, max_length=36
    )  # Field name made lowercase.
    ordervalue = models.IntegerField(
        db_column="OrderValue"
    )  # Field name made lowercase.
    isbuiltin = models.BooleanField(db_column="IsBuiltin")  # Field name made lowercase.
    parentgroupid = models.ForeignKey(
        "self", models.DO_NOTHING, db_column="ParentGroupID", blank=True, null=True
    )  # Field name made lowercase.
    grouppriority = models.IntegerField(
        db_column="GroupPriority"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbTargetGroup"


class Tbtargetgroupinautodeploymentrule(models.Model):
    autodeploymentruleid = models.OneToOneField(
        Tbautodeploymentrule,
        models.DO_NOTHING,
        db_column="AutoDeploymentRuleID",
        primary_key=True,
    )  # Field name made lowercase.
    targetgroupid = models.ForeignKey(
        Tbtargetgroup, models.DO_NOTHING, db_column="TargetGroupID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbTargetGroupInAutoDeploymentRule"
        unique_together = (("autodeploymentruleid", "targetgroupid"),)


class Tbtargetgrouptype(models.Model):
    targetgrouptypeid = models.AutoField(
        db_column="TargetGroupTypeID", primary_key=True
    )  # Field name made lowercase.
    name = models.CharField(
        db_column="Name", max_length=256
    )  # Field name made lowercase.
    description = models.CharField(
        db_column="Description", max_length=1024, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbTargetGroupType"


class Tbtargetintargetgroup(models.Model):
    targetgroupid = models.OneToOneField(
        Tbtargetgroup, models.DO_NOTHING, db_column="TargetGroupID", primary_key=True
    )  # Field name made lowercase.
    targetid = models.ForeignKey(
        Tbtarget, models.DO_NOTHING, db_column="TargetID"
    )  # Field name made lowercase.
    isexplicitmember = models.BooleanField(
        db_column="IsExplicitMember"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbTargetInTargetGroup"
        unique_together = (("targetgroupid", "targetid"),)


class Tbtargettype(models.Model):
    targettypeid = models.AutoField(
        db_column="TargetTypeID", primary_key=True
    )  # Field name made lowercase.
    name = models.CharField(
        db_column="Name", max_length=256
    )  # Field name made lowercase.
    description = models.CharField(
        db_column="Description", max_length=1024, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbTargetType"


class Tbtargeteddriverhwid(models.Model):
    drivertargetingid = models.OneToOneField(
        Tbdrivertargetinggroup,
        models.DO_NOTHING,
        db_column="DriverTargetingID",
        primary_key=True,
    )  # Field name made lowercase.
    localupdateid = models.ForeignKey(
        "Tbupdate", models.DO_NOTHING, db_column="LocalUpdateID"
    )  # Field name made lowercase.
    hardwareid = models.CharField(
        db_column="HardwareID", max_length=200
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbTargetedDriverHwid"
        unique_together = (("drivertargetingid", "localupdateid", "hardwareid"),)


class Tbupdate(models.Model):
    localupdateid = models.AutoField(
        db_column="LocalUpdateID", primary_key=True
    )  # Field name made lowercase.
    updateid = models.CharField(
        db_column="UpdateID", unique=True, max_length=36
    )  # Field name made lowercase.
    updatetypeid = models.ForeignKey(
        "Tbupdatetype", models.DO_NOTHING, db_column="UpdateTypeID"
    )  # Field name made lowercase.
    isclientselfupdate = models.BooleanField(
        db_column="IsClientSelfUpdate"
    )  # Field name made lowercase.
    publisherid = models.CharField(
        db_column="PublisherID", max_length=36
    )  # Field name made lowercase.
    ispublic = models.BooleanField(db_column="IsPublic")  # Field name made lowercase.
    ishidden = models.BooleanField(db_column="IsHidden")  # Field name made lowercase.
    detectoidtype = models.CharField(
        db_column="DetectoidType", max_length=80, blank=True, null=True
    )  # Field name made lowercase.
    legacyname = models.CharField(
        db_column="LegacyName", max_length=255, blank=True, null=True
    )  # Field name made lowercase.
    lastundeclinedtime = models.DateTimeField(
        db_column="LastUndeclinedTime", blank=True, null=True
    )  # Field name made lowercase.
    islocallypublished = models.BooleanField(
        db_column="IsLocallyPublished"
    )  # Field name made lowercase.
    importedtime = models.DateTimeField(
        db_column="ImportedTime"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbUpdate"


class Tbupdateclassificationinautodeploymentrule(models.Model):
    autodeploymentruleid = models.OneToOneField(
        Tbautodeploymentrule,
        models.DO_NOTHING,
        db_column="AutoDeploymentRuleID",
        primary_key=True,
    )  # Field name made lowercase.
    updateclassificationid = models.ForeignKey(
        Tbupdate, models.DO_NOTHING, db_column="UpdateClassificationID"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbUpdateClassificationInAutoDeploymentRule"
        unique_together = (("autodeploymentruleid", "updateclassificationid"),)


class Tbupdateflag(models.Model):
    localupdateid = models.IntegerField(
        db_column="LocalUpdateID", primary_key=True
    )  # Field name made lowercase.
    fromcatalogsite = models.BooleanField(
        db_column="FromCatalogSite", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbUpdateFlag"


class Tbupdatestatuspercomputer(models.Model):
    summarizationstate = models.IntegerField(
        db_column="SummarizationState"
    )  # Field name made lowercase.
    localupdateid = models.ForeignKey(
        Tbupdate, models.DO_NOTHING, db_column="LocalUpdateID"
    )  # Field name made lowercase.
    targetid = models.OneToOneField(
        Tbcomputertarget, models.DO_NOTHING, db_column="TargetID", primary_key=True
    )  # Field name made lowercase.
    lastchangetime = models.DateTimeField(
        db_column="LastChangeTime"
    )  # Field name made lowercase.
    lastrefreshtime = models.DateTimeField(
        db_column="LastRefreshTime"
    )  # Field name made lowercase.
    lastchangetimeonserver = models.DateTimeField(
        db_column="LastChangeTimeOnServer"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbUpdateStatusPerComputer"
        unique_together = (("targetid", "localupdateid"),)


class Tbupdatesummaryforallcomputers(models.Model):
    localupdateid = models.OneToOneField(
        Tbupdate, models.DO_NOTHING, db_column="LocalUpdateID", primary_key=True
    )  # Field name made lowercase.
    unknown = models.IntegerField(db_column="Unknown")  # Field name made lowercase.
    notinstalled = models.IntegerField(
        db_column="NotInstalled"
    )  # Field name made lowercase.
    downloaded = models.IntegerField(
        db_column="Downloaded"
    )  # Field name made lowercase.
    installed = models.IntegerField(db_column="Installed")  # Field name made lowercase.
    failed = models.IntegerField(db_column="Failed")  # Field name made lowercase.
    installedpendingreboot = models.IntegerField(
        db_column="InstalledPendingReboot"
    )  # Field name made lowercase.
    lastchangetime = models.DateTimeField(
        db_column="LastChangeTime"
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbUpdateSummaryForAllComputers"


class Tbupdatetype(models.Model):
    updatetypeid = models.CharField(
        db_column="UpdateTypeID", primary_key=True, max_length=36
    )  # Field name made lowercase.
    name = models.CharField(
        db_column="Name", max_length=256
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbUpdateType"


class Tbxml(models.Model):
    xmlid = models.AutoField(
        db_column="XmlID", primary_key=True
    )  # Field name made lowercase.
    rootelementxml = models.TextField(
        db_column="RootElementXml"
    )  # Field name made lowercase.
    rootelementtype = models.IntegerField(
        db_column="RootElementType"
    )  # Field name made lowercase.
    languageid = models.IntegerField(
        db_column="LanguageID"
    )  # Field name made lowercase.
    revisionid = models.ForeignKey(
        Tbrevision, models.DO_NOTHING, db_column="RevisionID"
    )  # Field name made lowercase.
    rootelementxmlcompressed = models.BinaryField(
        db_column="RootElementXmlCompressed", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = "tbXml"


class Vwapiupdatetype(models.Model):
    updateid = models.CharField(
        db_column="UpdateID", max_length=36
    )  # Field name made lowercase.
    localupdateid = models.IntegerField(
        db_column="LocalUpdateID"
    )  # Field name made lowercase.
    revisionid = models.IntegerField(
        db_column="RevisionID"
    )  # Field name made lowercase.
    updatetypename = models.CharField(
        db_column="UpdateTypeName", max_length=256
    )  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = "vwApiUpdateType"


class Vwcategoryproperties(models.Model):
    localupdateid = models.IntegerField(
        db_column="LocalUpdateID"
    )  # Field name made lowercase.
    updateid = models.CharField(
        db_column="UpdateID", max_length=36
    )  # Field name made lowercase.
    revisionid = models.IntegerField(
        db_column="RevisionID"
    )  # Field name made lowercase.
    categorytype = models.CharField(
        db_column="CategoryType", max_length=256
    )  # Field name made lowercase.
    prohibitssubcategories = models.BooleanField(
        db_column="ProhibitsSubcategories"
    )  # Field name made lowercase.
    prohibitsupdates = models.BooleanField(
        db_column="ProhibitsUpdates"
    )  # Field name made lowercase.
    categoryid = models.IntegerField(
        db_column="CategoryID"
    )  # Field name made lowercase.
    parentcategoryid = models.IntegerField(
        db_column="ParentCategoryID", blank=True, null=True
    )  # Field name made lowercase.
    categoryindex = models.IntegerField(
        db_column="CategoryIndex"
    )  # Field name made lowercase.
    displayorder = models.IntegerField(
        db_column="DisplayOrder", blank=True, null=True
    )  # Field name made lowercase.
    arrivaldate = models.DateTimeField(
        db_column="ArrivalDate"
    )  # Field name made lowercase.
    updatesource = models.IntegerField(
        db_column="UpdateSource", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = "vwCategoryProperties"


class Vwconfiguration(models.Model):
    configurationid = models.IntegerField(
        db_column="ConfigurationID"
    )  # Field name made lowercase.
    lastconfigchange = models.DateTimeField(
        db_column="LastConfigChange"
    )  # Field name made lowercase.
    dssanonymoustargeting = models.BooleanField(
        db_column="DssAnonymousTargeting"
    )  # Field name made lowercase.
    isregistrationrequired = models.BooleanField(
        db_column="IsRegistrationRequired"
    )  # Field name made lowercase.
    maxdeltasyncperiod = models.IntegerField(
        db_column="MaxDeltaSyncPeriod"
    )  # Field name made lowercase.
    reportingserviceurl = models.CharField(
        db_column="ReportingServiceUrl", max_length=1024
    )  # Field name made lowercase.
    serverid = models.CharField(
        db_column="ServerID", max_length=36
    )  # Field name made lowercase.
    anonymouscookieexpirationtime = models.BigIntegerField(
        db_column="AnonymousCookieExpirationTime", blank=True, null=True
    )  # Field name made lowercase.
    simpletargetingcookieexpirationtime = models.BigIntegerField(
        db_column="SimpleTargetingCookieExpirationTime"
    )  # Field name made lowercase.
    maximumservercookieexpirationtime = models.BigIntegerField(
        db_column="MaximumServerCookieExpirationTime"
    )  # Field name made lowercase.
    dsstargetingcookieexpirationtime = models.BigIntegerField(
        db_column="DssTargetingCookieExpirationTime"
    )  # Field name made lowercase.
    encryptionkey = models.BinaryField(
        db_column="EncryptionKey"
    )  # Field name made lowercase.
    servertargeting = models.BooleanField(
        db_column="ServerTargeting"
    )  # Field name made lowercase.
    synctomu = models.BooleanField(db_column="SyncToMU")  # Field name made lowercase.
    upstreamservername = models.CharField(
        db_column="UpstreamServerName", max_length=256
    )  # Field name made lowercase.
    serverportnumber = models.IntegerField(
        db_column="ServerPortNumber"
    )  # Field name made lowercase.
    upstreamserverusessl = models.BooleanField(
        db_column="UpstreamServerUseSSL"
    )  # Field name made lowercase.
    useproxy = models.BooleanField(db_column="UseProxy")  # Field name made lowercase.
    proxyname = models.CharField(
        db_column="ProxyName", max_length=256
    )  # Field name made lowercase.
    proxyserverport = models.IntegerField(
        db_column="ProxyServerPort"
    )  # Field name made lowercase.
    anonymousproxyaccess = models.BooleanField(
        db_column="AnonymousProxyAccess"
    )  # Field name made lowercase.
    proxyusername = models.CharField(
        db_column="ProxyUserName", max_length=256
    )  # Field name made lowercase.
    hostonmu = models.BooleanField(db_column="HostOnMu")  # Field name made lowercase.
    localcontentcachelocation = models.CharField(
        db_column="LocalContentCacheLocation", max_length=256
    )  # Field name made lowercase.
    serversupportsalllanguages = models.BooleanField(
        db_column="ServerSupportsAllLanguages"
    )  # Field name made lowercase.
    loglevel = models.IntegerField(db_column="LogLevel")  # Field name made lowercase.
    logpath = models.CharField(
        db_column="LogPath", max_length=256
    )  # Field name made lowercase.
    subscriptionfailurenumberofretries = models.IntegerField(
        db_column="SubscriptionFailureNumberOfRetries"
    )  # Field name made lowercase.
    subscriptionfailurewaitbetweenretriestime = models.BigIntegerField(
        db_column="SubscriptionFailureWaitBetweenRetriesTime"
    )  # Field name made lowercase.
    dispatchmanagerpollinginterval = models.BigIntegerField(
        db_column="DispatchManagerPollingInterval"
    )  # Field name made lowercase.
    statemachinetransitionloggingenabled = models.BooleanField(
        db_column="StateMachineTransitionLoggingEnabled"
    )  # Field name made lowercase.
    statemachinetransitionerrorcapturelength = models.BigIntegerField(
        db_column="StateMachineTransitionErrorCaptureLength"
    )  # Field name made lowercase.
    maxsimultaneousfiledownloads = models.IntegerField(
        db_column="MaxSimultaneousFileDownloads"
    )  # Field name made lowercase.
    muurl = models.CharField(
        db_column="MUUrl", max_length=1024
    )  # Field name made lowercase.
    eventlogfloodprotecttime = models.BigIntegerField(
        db_column="EventLogFloodProtectTime"
    )  # Field name made lowercase.
    handshakeanchor = models.CharField(
        db_column="HandshakeAnchor", max_length=64, blank=True, null=True
    )  # Field name made lowercase.
    statsdotnetwebserviceuri = models.CharField(
        db_column="StatsDotNetWebServiceUri", max_length=1024
    )  # Field name made lowercase.
    queueflushtimeinms = models.IntegerField(
        db_column="QueueFlushTimeInMS"
    )  # Field name made lowercase.
    queueflushcount = models.IntegerField(
        db_column="QueueFlushCount"
    )  # Field name made lowercase.
    queuerejectcount = models.IntegerField(
        db_column="QueueRejectCount"
    )  # Field name made lowercase.
    sleeptimeaftererrorinms = models.IntegerField(
        db_column="SleepTimeAfterErrorInMS"
    )  # Field name made lowercase.
    logdestinations = models.IntegerField(
        db_column="LogDestinations"
    )  # Field name made lowercase.
    autorefreshdeployments = models.BooleanField(
        db_column="AutoRefreshDeployments"
    )  # Field name made lowercase.
    redirectorchangenumber = models.BigIntegerField(
        db_column="RedirectorChangeNumber"
    )  # Field name made lowercase.
    importlocalpath = models.CharField(
        db_column="ImportLocalPath", max_length=256
    )  # Field name made lowercase.
    usecookievalidation = models.BooleanField(
        db_column="UseCookieValidation"
    )  # Field name made lowercase.
    autopurgeclienteventagethreshold = models.IntegerField(
        db_column="AutoPurgeClientEventAgeThreshold", blank=True, null=True
    )  # Field name made lowercase.
    autopurgeservereventagethreshold = models.IntegerField(
        db_column="AutoPurgeServerEventAgeThreshold", blank=True, null=True
    )  # Field name made lowercase.
    autopurgedetectionperiod = models.IntegerField(
        db_column="AutoPurgeDetectionPeriod", blank=True, null=True
    )  # Field name made lowercase.
    doreportingdatavalidation = models.BooleanField(
        db_column="DoReportingDataValidation"
    )  # Field name made lowercase.
    doreportingsummarization = models.BooleanField(
        db_column="DoReportingSummarization"
    )  # Field name made lowercase.
    maxnumberofidstorequestdatafromuss = models.IntegerField(
        db_column="MaxNumberOfIdsToRequestDataFromUss"
    )  # Field name made lowercase.
    maxcoreupdatesperrequest = models.IntegerField(
        db_column="MaxCoreUpdatesPerRequest"
    )  # Field name made lowercase.
    maxextendedupdatesperrequest = models.IntegerField(
        db_column="MaxExtendedUpdatesPerRequest"
    )  # Field name made lowercase.
    downloadregulationurl = models.CharField(
        db_column="DownloadRegulationUrl", max_length=1024
    )  # Field name made lowercase.
    allowproxycredentialsovernonssl = models.BooleanField(
        db_column="AllowProxyCredentialsOverNonSsl"
    )  # Field name made lowercase.
    lazysync = models.BooleanField(db_column="LazySync")  # Field name made lowercase.
    downloadexpresspackages = models.BooleanField(
        db_column="DownloadExpressPackages"
    )  # Field name made lowercase.
    doserversynccompression = models.BooleanField(
        db_column="DoServerSyncCompression"
    )  # Field name made lowercase.
    proxyuserdomain = models.CharField(
        db_column="ProxyUserDomain", max_length=256
    )  # Field name made lowercase.
    bitshealthscanninginterval = models.BigIntegerField(
        db_column="BitsHealthScanningInterval"
    )  # Field name made lowercase.
    bitsdownloadpriorityforeground = models.BooleanField(
        db_column="BitsDownloadPriorityForeground"
    )  # Field name made lowercase.
    maxxmlperrequest = models.IntegerField(
        db_column="MaxXmlPerRequest"
    )  # Field name made lowercase.
    maxxmlperrequestinserversync = models.IntegerField(
        db_column="MaxXmlPerRequestInServerSync"
    )  # Field name made lowercase.
    maxtargetcomputers = models.IntegerField(
        db_column="MaxTargetComputers"
    )  # Field name made lowercase.
    maxeventinstances = models.IntegerField(
        db_column="MaxEventInstances"
    )  # Field name made lowercase.
    logrolloverfilesizeinbytes = models.IntegerField(
        db_column="LogRolloverFileSizeInBytes"
    )  # Field name made lowercase.
    wusinstalltype = models.IntegerField(
        db_column="WUSInstallType"
    )  # Field name made lowercase.
    replicamode = models.BooleanField(
        db_column="ReplicaMode"
    )  # Field name made lowercase.
    autodeploymandatory = models.BooleanField(
        db_column="AutoDeployMandatory"
    )  # Field name made lowercase.
    deploymentchangedeferral = models.IntegerField(
        db_column="DeploymentChangeDeferral"
    )  # Field name made lowercase.
    revisiondeletiontimethreshold = models.IntegerField(
        db_column="RevisionDeletionTimeThreshold"
    )  # Field name made lowercase.
    revisiondeletionsizethreshold = models.IntegerField(
        db_column="RevisionDeletionSizeThreshold"
    )  # Field name made lowercase.
    collectclientinventory = models.BooleanField(
        db_column="CollectClientInventory"
    )  # Field name made lowercase.
    dodetailedrollup = models.BooleanField(
        db_column="DoDetailedRollup"
    )  # Field name made lowercase.
    rollupresetguid = models.CharField(
        db_column="RollupResetGuid", max_length=36
    )  # Field name made lowercase.
    usssupportsalllanguages = models.BooleanField(
        db_column="UssSupportsAllLanguages", blank=True, null=True
    )  # Field name made lowercase.
    getcontentfrommu = models.BooleanField(
        db_column="GetContentFromMU", blank=True, null=True
    )  # Field name made lowercase.
    hmdetectintervalinseconds = models.IntegerField(
        db_column="HmDetectIntervalInSeconds"
    )  # Field name made lowercase.
    hmrefreshintervalinseconds = models.IntegerField(
        db_column="HmRefreshIntervalInSeconds"
    )  # Field name made lowercase.
    hmcorediskspacegreenmegabytes = models.IntegerField(
        db_column="HmCoreDiskSpaceGreenMegabytes"
    )  # Field name made lowercase.
    hmcorediskspaceredmegabytes = models.IntegerField(
        db_column="HmCoreDiskSpaceRedMegabytes"
    )  # Field name made lowercase.
    hmcorecatalogsyncintervalindays = models.IntegerField(
        db_column="HmCoreCatalogSyncIntervalInDays"
    )  # Field name made lowercase.
    hmclientsinstallupdatesgreenpercent = models.IntegerField(
        db_column="HmClientsInstallUpdatesGreenPercent"
    )  # Field name made lowercase.
    hmclientsinstallupdatesredpercent = models.IntegerField(
        db_column="HmClientsInstallUpdatesRedPercent"
    )  # Field name made lowercase.
    hmclientsinventorygreenpercent = models.IntegerField(
        db_column="HmClientsInventoryGreenPercent"
    )  # Field name made lowercase.
    hmclientsinventoryredpercent = models.IntegerField(
        db_column="HmClientsInventoryRedPercent"
    )  # Field name made lowercase.
    hmclientsinventoryscandiffinhours = models.IntegerField(
        db_column="HmClientsInventoryScanDiffInHours"
    )  # Field name made lowercase.
    hmclientssilentgreenpercent = models.IntegerField(
        db_column="HmClientsSilentGreenPercent"
    )  # Field name made lowercase.
    hmclientssilentredpercent = models.IntegerField(
        db_column="HmClientsSilentRedPercent"
    )  # Field name made lowercase.
    hmclientssilentdays = models.IntegerField(
        db_column="HmClientsSilentDays"
    )  # Field name made lowercase.
    dssrollupchunksize = models.TextField(
        db_column="DssRollupChunkSize"
    )  # Field name made lowercase. This field type is a guess.
    murollupoptin = models.TextField(
        db_column="MURollupOptin"
    )  # Field name made lowercase. This field type is a guess.
    autorefreshdeploymentsdeclineexpired = models.BooleanField(
        db_column="AutoRefreshDeploymentsDeclineExpired", blank=True, null=True
    )  # Field name made lowercase.
    serverstring = models.TextField(
        db_column="ServerString"
    )  # Field name made lowercase. This field type is a guess.
    hmcoreflags = models.IntegerField(
        db_column="HmCoreFlags"
    )  # Field name made lowercase.
    hmclientsflags = models.IntegerField(
        db_column="HmClientsFlags"
    )  # Field name made lowercase.
    hmdatabaseflags = models.IntegerField(
        db_column="HmDatabaseFlags"
    )  # Field name made lowercase.
    hmwebservicesflags = models.IntegerField(
        db_column="HmWebServicesFlags"
    )  # Field name made lowercase.
    clientreportinglevel = models.TextField(
        db_column="ClientReportingLevel"
    )  # Field name made lowercase. This field type is a guess.
    localpublishingmaxcabsize = models.IntegerField(
        db_column="LocalPublishingMaxCabSize", blank=True, null=True
    )  # Field name made lowercase.
    downloadregulationwebserviceurl = models.CharField(
        db_column="DownloadRegulationWebServiceUrl",
        max_length=30,
        blank=True,
        null=True,
    )  # Field name made lowercase.
    loadodflocally = models.BooleanField(
        db_column="LoadOdfLocally", blank=True, null=True
    )  # Field name made lowercase.
    odffilepath = models.CharField(
        db_column="OdfFilePath", max_length=30, blank=True, null=True
    )  # Field name made lowercase.
    hmclientstoomanygreenpercent = models.IntegerField(
        db_column="HmClientsTooManyGreenPercent"
    )  # Field name made lowercase.
    hmclientstoomanyredpercent = models.IntegerField(
        db_column="HmClientsTooManyRedPercent"
    )  # Field name made lowercase.
    computerdeletiontimethreshold = models.IntegerField(
        db_column="ComputerDeletionTimeThreshold", blank=True, null=True
    )  # Field name made lowercase.
    configurationchangenumber = models.BigIntegerField(
        db_column="ConfigurationChangeNumber"
    )  # Field name made lowercase.
    useseparateproxyforssl = models.BooleanField(
        db_column="UseSeparateProxyForSsl", blank=True, null=True
    )  # Field name made lowercase.
    sslproxyname = models.CharField(
        db_column="SslProxyName", max_length=30, blank=True, null=True
    )  # Field name made lowercase.
    sslproxyserverport = models.IntegerField(
        db_column="SslProxyServerPort", blank=True, null=True
    )  # Field name made lowercase.
    serversupportsallavailablelanguages = models.BooleanField(
        db_column="ServerSupportsAllAvailableLanguages", blank=True, null=True
    )  # Field name made lowercase.
    oobeinitialized = models.BooleanField(
        db_column="OobeInitialized", blank=True, null=True
    )  # Field name made lowercase.
    trackdynamiccategories = models.BooleanField(
        db_column="TrackDynamicCategories", blank=True, null=True
    )  # Field name made lowercase.
    maxupdatesperrequestingetupdatedecryptiondata = models.IntegerField(
        db_column="MaxUpdatesPerRequestInGetUpdateDecryptionData", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = "vwConfiguration"


class Vwdefaultlocalizedproperty(models.Model):
    revisionid = models.IntegerField(
        db_column="RevisionID"
    )  # Field name made lowercase.
    localupdateid = models.IntegerField(
        db_column="LocalUpdateID"
    )  # Field name made lowercase.
    revisionnumber = models.IntegerField(
        db_column="RevisionNumber"
    )  # Field name made lowercase.
    islatestrevision = models.BooleanField(
        db_column="IsLatestRevision"
    )  # Field name made lowercase.
    title = models.CharField(
        db_column="Title", max_length=200
    )  # Field name made lowercase.
    description = models.CharField(
        db_column="Description", max_length=1500, blank=True, null=True
    )  # Field name made lowercase.
    releasenotes = models.CharField(
        db_column="ReleaseNotes", max_length=1000, blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = "vwDefaultLocalizedProperty"


class Vweventhistory(models.Model):
    eventinstanceid = models.CharField(
        db_column="EventInstanceID", max_length=36
    )  # Field name made lowercase.
    eventid = models.SmallIntegerField(
        db_column="EventID"
    )  # Field name made lowercase.
    eventnamespaceid = models.IntegerField(
        db_column="EventNamespaceID"
    )  # Field name made lowercase.
    eventsourceid = models.SmallIntegerField(
        db_column="EventSourceID"
    )  # Field name made lowercase.
    timeattarget = models.DateTimeField(
        db_column="TimeAtTarget"
    )  # Field name made lowercase.
    timeatserver = models.DateTimeField(
        db_column="TimeAtServer", blank=True, null=True
    )  # Field name made lowercase.
    stateid = models.IntegerField(db_column="StateID")  # Field name made lowercase.
    severityid = models.IntegerField(
        db_column="SeverityID"
    )  # Field name made lowercase.
    win32hresult = models.IntegerField(
        db_column="Win32HResult"
    )  # Field name made lowercase.
    messagetemplate = models.CharField(
        db_column="MessageTemplate", max_length=2048
    )  # Field name made lowercase.
    miscdataxml = models.TextField(
        db_column="MiscDataXml", blank=True, null=True
    )  # Field name made lowercase.
    replacementstringsxml = models.TextField(
        db_column="ReplacementStringsXml", blank=True, null=True
    )  # Field name made lowercase.
    appname = models.CharField(
        db_column="AppName", max_length=256, blank=True, null=True
    )  # Field name made lowercase.
    computerid = models.CharField(
        db_column="ComputerID", max_length=256, blank=True, null=True
    )  # Field name made lowercase.
    updateid = models.CharField(
        db_column="UpdateID", max_length=36, blank=True, null=True
    )  # Field name made lowercase.
    revisionnumber = models.IntegerField(
        db_column="RevisionNumber", blank=True, null=True
    )  # Field name made lowercase.
    deviceid = models.TextField(
        db_column="DeviceID", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = "vwEventHistory"


class Vwminimalupdate(models.Model):
    updateid = models.CharField(
        db_column="UpdateID", max_length=36
    )  # Field name made lowercase.
    revisionnumber = models.IntegerField(
        db_column="RevisionNumber"
    )  # Field name made lowercase.
    revisionid = models.IntegerField(
        db_column="RevisionID"
    )  # Field name made lowercase.
    localupdateid = models.IntegerField(
        db_column="LocalUpdateID"
    )  # Field name made lowercase.
    isleaf = models.BooleanField(db_column="IsLeaf")  # Field name made lowercase.
    installationsupported = models.BooleanField(
        db_column="InstallationSupported", blank=True, null=True
    )  # Field name made lowercase.
    installationimpact = models.IntegerField(
        db_column="InstallationImpact", blank=True, null=True
    )  # Field name made lowercase.
    installationrebootbehavior = models.IntegerField(
        db_column="InstallationRebootBehavior", blank=True, null=True
    )  # Field name made lowercase.
    installationrequiresnetworkconnectivity = models.BooleanField(
        db_column="InstallationRequiresNetworkConnectivity", blank=True, null=True
    )  # Field name made lowercase.
    installationcanrequestuserinput = models.BooleanField(
        db_column="InstallationCanRequestUserInput", blank=True, null=True
    )  # Field name made lowercase.
    uninstallationsupported = models.BooleanField(
        db_column="UninstallationSupported", blank=True, null=True
    )  # Field name made lowercase.
    uninstallationimpact = models.IntegerField(
        db_column="UninstallationImpact", blank=True, null=True
    )  # Field name made lowercase.
    uninstallationrebootbehavior = models.IntegerField(
        db_column="UninstallationRebootBehavior", blank=True, null=True
    )  # Field name made lowercase.
    uninstallationrequiresnetworkconnectivity = models.BooleanField(
        db_column="UninstallationRequiresNetworkConnectivity", blank=True, null=True
    )  # Field name made lowercase.
    uninstallationcanrequestuserinput = models.BooleanField(
        db_column="UninstallationCanRequestUserInput", blank=True, null=True
    )  # Field name made lowercase.
    updatetype = models.CharField(
        db_column="UpdateType", max_length=256
    )  # Field name made lowercase.
    publicationstate = models.IntegerField(
        db_column="PublicationState"
    )  # Field name made lowercase.
    creationdate = models.DateTimeField(
        db_column="CreationDate"
    )  # Field name made lowercase.
    state = models.SmallIntegerField(db_column="State")  # Field name made lowercase.
    eulaid = models.CharField(
        db_column="EulaID", max_length=36, blank=True, null=True
    )  # Field name made lowercase.
    requireseulaacceptance = models.BooleanField(
        db_column="RequiresEulaAcceptance", blank=True, null=True
    )  # Field name made lowercase.
    declined = models.BooleanField(db_column="Declined")  # Field name made lowercase.
    hasstaledeployments = models.BooleanField(
        db_column="HasStaleDeployments", blank=True, null=True
    )  # Field name made lowercase.
    islatestrevision = models.BooleanField(
        db_column="IsLatestRevision"
    )  # Field name made lowercase.
    receivedfromcreatorservice = models.DateTimeField(
        db_column="ReceivedFromCreatorService"
    )  # Field name made lowercase.
    hassupersededupdates = models.BooleanField(
        db_column="HasSupersededUpdates", blank=True, null=True
    )  # Field name made lowercase.
    legacyname = models.CharField(
        db_column="LegacyName", max_length=255, blank=True, null=True
    )  # Field name made lowercase.
    msrcseverity = models.CharField(
        db_column="MsrcSeverity", max_length=20
    )  # Field name made lowercase.
    hasearlierrevision = models.BooleanField(
        db_column="HasEarlierRevision", blank=True, null=True
    )  # Field name made lowercase.
    ismandatory = models.BooleanField(
        db_column="IsMandatory"
    )  # Field name made lowercase.
    issuperseded = models.BooleanField(
        db_column="IsSuperseded", blank=True, null=True
    )  # Field name made lowercase.
    iseditable = models.BooleanField(
        db_column="IsEditable", blank=True, null=True
    )  # Field name made lowercase.
    effectivearrivaltimeforreporting = models.DateTimeField(
        db_column="EffectiveArrivalTimeForReporting"
    )  # Field name made lowercase.
    updatesource = models.IntegerField(
        db_column="UpdateSource", blank=True, null=True
    )  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = "vwMinimalUpdate"


class Vwstatemachineeventtransitionlog(models.Model):
    entryid = models.IntegerField(db_column="EntryID")  # Field name made lowercase.
    eventtime = models.DateTimeField(
        db_column="EventTime"
    )  # Field name made lowercase.
    statemachinename = models.CharField(
        db_column="StateMachineName", max_length=256
    )  # Field name made lowercase.
    rowid = models.CharField(
        db_column="RowID", max_length=36
    )  # Field name made lowercase.
    oldstate = models.CharField(
        db_column="OldState", max_length=256
    )  # Field name made lowercase.
    eventname = models.CharField(
        db_column="EventName", max_length=256
    )  # Field name made lowercase.
    newstate = models.CharField(
        db_column="NewState", max_length=256
    )  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = "vwStateMachineEventTransitionLog"


class Vwupdateincategory(models.Model):
    updateid = models.CharField(
        db_column="UpdateID", max_length=36
    )  # Field name made lowercase.
    localupdateid = models.IntegerField(
        db_column="LocalUpdateID"
    )  # Field name made lowercase.
    categoryid = models.IntegerField(
        db_column="CategoryID"
    )  # Field name made lowercase.
    categoryupdateid = models.CharField(
        db_column="CategoryUpdateID", max_length=36
    )  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = "vwUpdateInCategory"


class Vwupdatelanguage(models.Model):
    updateid = models.CharField(
        db_column="UpdateID", max_length=36
    )  # Field name made lowercase.
    revisionnumber = models.IntegerField(
        db_column="RevisionNumber"
    )  # Field name made lowercase.
    language = models.CharField(
        db_column="Language", max_length=16
    )  # Field name made lowercase.
    isleaf = models.BooleanField(db_column="IsLeaf")  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = "vwUpdateLanguage"


class Vwupdatelocalizedproperties(models.Model):
    updateid = models.CharField(
        db_column="UpdateID", max_length=36
    )  # Field name made lowercase.
    revisionnumber = models.IntegerField(
        db_column="RevisionNumber"
    )  # Field name made lowercase.
    shortlanguage = models.CharField(
        db_column="ShortLanguage", max_length=16
    )  # Field name made lowercase.
    languageid = models.IntegerField(
        db_column="LanguageID"
    )  # Field name made lowercase.
    title = models.CharField(
        db_column="Title", max_length=200
    )  # Field name made lowercase.
    description = models.CharField(
        db_column="Description", max_length=1500, blank=True, null=True
    )  # Field name made lowercase.
    releasenotes = models.CharField(
        db_column="ReleaseNotes", max_length=1000, blank=True, null=True
    )  # Field name made lowercase.
    isleaf = models.BooleanField(db_column="IsLeaf")  # Field name made lowercase.
    revisionid = models.IntegerField(
        db_column="RevisionID"
    )  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = "vwUpdateLocalizedProperties"


class Vwupdatestoreport(models.Model):
    updateid = models.CharField(
        db_column="UpdateID", max_length=36
    )  # Field name made lowercase.
    localupdateid = models.IntegerField(
        db_column="LocalUpdateID"
    )  # Field name made lowercase.
    revisionid = models.IntegerField(
        db_column="RevisionID"
    )  # Field name made lowercase.
    lastundeclinedtime = models.DateTimeField(
        db_column="LastUndeclinedTime", blank=True, null=True
    )  # Field name made lowercase.
    importedtime = models.DateTimeField(
        db_column="ImportedTime"
    )  # Field name made lowercase.

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = "vwUpdatesToReport"
